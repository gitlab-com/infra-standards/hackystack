<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudKeypairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_keypairs', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->nullable()->index();
            $table->uuid('cloud_account_id')->nullable()->index();
            $table->uuid('cloud_realm_id')->nullable()->index();
            $table->json('api_meta_data')->nullable();
            $table->string('name');
            $table->string('slug')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('fingerprint')->nullable();
            $table->text('public_key')->nullable();
            $table->text('private_key')->nullable();
            $table->boolean('flag_provisioned')->default(false)->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('provisioned_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
            $table->unique(['cloud_account_id', 'slug'], 'cloud_keypairs_account_slug_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_keypairs');
    }
}
