<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGitCredentialsToAuthUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('git_username', 'auth_users')) {
            return;
        }

        Schema::table('auth_users', function (Blueprint $table) {
            $table->string('git_username')->nullable()->after('provider_token');
            $table->text('git_password')->nullable()->after('git_username')->comment('encrypted');
            $table->json('git_meta_data')->nullable()->after('git_password');
            $table->timestamp('git_user_provisioned_at')->nullable()->after('last_activity_at');
            $table->timestamp('git_user_deprovisioned_at')->nullable()->after('git_user_provisioned_at');
            $table->boolean('flag_git_user_provisioned')->default(false)->nullable()->after('flag_privacy_accepted');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auth_users', function (Blueprint $table) {
            $table->dropColumn('git_username');
            $table->dropColumn('git_password');
            $table->dropColumn('git_meta_data');
            $table->dropColumn('git_user_provisioned_at');
            $table->dropColumn('git_user_deprovisioned_at');
            $table->dropColumn('flag_git_user_provisioned');
        });
    }
}
