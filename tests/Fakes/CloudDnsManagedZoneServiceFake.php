<?php

namespace Tests\Fakes;

use App\Services\V1\Vendor\Gcp\CloudDnsManagedZoneService;

class CloudDnsManagedZoneServiceFake extends CloudDnsManagedZoneService
{
    public function checkForDnsZone(array $request_data): object|string
    {
        return parent::checkForDnsZone($request_data); // TODO: Change the autogenerated stub
    }

    public function createDnsZoneName(string $zone_name): string
    {
        return parent::createDnsZoneName($zone_name); // TODO: Change the autogenerated stub
    }
}
