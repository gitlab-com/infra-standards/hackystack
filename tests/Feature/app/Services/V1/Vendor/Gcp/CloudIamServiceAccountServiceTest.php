<?php

namespace Tests\Feature\app\Services\V1\Vendor\Gcp;

use App\Services\V1\Vendor\Gcp\CloudIamServiceAccountService;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\Fakes\CloudIamServiceAccountServiceFake;
use TypeError;

it('initializes the service with a valid cloud provider and can generate an API token', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));

    expect($service)->toBeInstanceOf(CloudIamServiceAccountServiceFake::class);
});

it('cannot initialize the service with an invalid cloud provider', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'aws')->first();

    if (!$gcp_cloud_provider) {
        throw new Exception('No Cloud Provider with the `aws` type exists in the database.');
    }

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
})->expectExceptionMessage('The GCP service is trying to use a cloud provider that is not the correct type.');

it('can set the project_id class variable to a numeric value', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_id');

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, $project_id);

    $service->setProjectId($project_id);

    expect($service->getProjectId())->toBe($project_id);
});

it('can set the project_id class variable to an alpha-dash value', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $project_id = config('tests.gcp.project_name');

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, $project_id);

    $service->setProjectId($project_id);

    expect($service->getProjectId())->toBe($project_id);
});

it('cannot set the project_id class variable as an array and throws a type error', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->first();

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, [config('tests.gcp.project_name')]);
})->expectException('TypeError');

it('can get a list of service accounts', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $list = $service->list();

    if ($list->status->code != 200) {
        throw new Exception($list->object->error->message);
    }

    $list_array = (array) $list->object->accounts;

    expect($list->status->code)->toBe(200);
    expect($list->object)->toBeObject();
    expect($list_array[0]->name)->toStartWith('projects');
});

it('can get a specific service account', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $accounts = (array) $service->list()->object->accounts;

    $service_account = $service->get($accounts[0]->uniqueId);
    //$service_account = $service->get($accounts[0]->email);

    if ($service_account->status->code != 200) {
        throw new Exception($service_account->object->error->message);
    }

    expect($service_account->status->code)->toBe(200);
    expect($service_account->object->name)->toStartWith('projects');
});

it('can create a new service account', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $random_suffix = rand(1000, 9999);

    $service_account = $service->store([
        'account_id' => config('tests.gcp.service_account_id_prefix') . $random_suffix,
        'display_name' => config('tests.gcp.service_account_display_name') . ' ' . $random_suffix,
        'description' => config('tests.gcp.service_account_description') . ' (store)'
    ]);

    if ($service_account->status->code != 200) {
        throw new \Exception($service_account->object->error->message);
    }

    expect($service_account->status->code)->toBe(200);
    expect($service_account->object->description)->toStartWith(config('tests.gcp.service_account_description'));

    if ($service_account->status->code == 200) {
        $service->delete($service_account->object->uniqueId);
    }
});

it('can update a service account description', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $random_suffix = rand(1000, 9999);

    $service_account = $service->store([
        'account_id' => config('tests.gcp.service_account_id_prefix') . $random_suffix,
        'display_name' => config('tests.gcp.service_account_display_name') . ' ' . $random_suffix,
        'description' => config('tests.gcp.service_account_description') . ' (update)'
    ]);

    if ($service_account->status->code != 200) {
        throw new Exception($service_account->object->error->message);
    } else {
        $updated_service_account = $service->update($service_account->object->uniqueId, [
            'description' => 'The description was successfully updated by the unit test.'
        ]);

        if ($updated_service_account->status->code != 200) {
            throw new Exception($updated_service_account->object->error->message);
        }

        expect($updated_service_account->status->code)->toBe(200);
        expect($updated_service_account->object->description)->toBe('The description was successfully updated by the unit test.');

        $service->delete($service_account->object->uniqueId);
    }
});

it('can delete a service account', function () {
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();

    $service = new CloudIamServiceAccountServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_name'));

    $random_suffix = rand(1000, 9999);

    $service_account = $service->store([
        'account_id' => config('tests.gcp.service_account_id_prefix') . $random_suffix,
        'display_name' => config('tests.gcp.service_account_display_name') . ' ' . $random_suffix,
        'description' => config('tests.gcp.service_account_description') . ' (delete)'
    ]);

    if ($service_account->status->code != 200) {
        throw new Exception($service_account->object->error->message);
    } else {
        $deleted_service_account = $service->delete($service_account->object->uniqueId);
        expect($deleted_service_account->status->code)->toBe(200);
    }
});
