#        _   _               _            ____   _                _           #
#       | | | |  __ _   ___ | | __ _   _ / ___| | |_  __ _   ___ | | __       #
#       | |_| | / _` | / __|| |/ /| | | |\___ \ | __|/ _` | / __|| |/ /       #
#       |  _  || (_| || (__ |   < | |_| | ___) || |_| (_| || (__ |   <        #
#       |_| |_| \__,_| \___||_|\_\ \__, ||____/  \__|\__,_| \___||_|\_\       #
#                                  |___/                                      #
# --------------------------------------------------------------------------- #
# This .env file has the environment variables for this HackyStack instance.  #
# --------------------------------------------------------------------------- #

# -----------------------------------------------------------------------------
#  Application Configuration
#  config/app.php
# -----------------------------------------------------------------------------

APP_NAME="HackyStack"
APP_LOGO="hackystack-icon.svg"
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=

# -----------------------------------------------------------------------------
#  Database Configuration
#  config/database.php
# -----------------------------------------------------------------------------

DB_CONNECTION=mysql
DB_HOST=
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

# -----------------------------------------------------------------------------
#  Error Reporting Configuration
# -----------------------------------------------------------------------------

LOG_CHANNEL=stack

BUGSNAG_API_KEY=

SENTRY_LARAVEL_DSN=
SENTRY_TRACES_SAMPLE_RATE=1.0

# -----------------------------------------------------------------------------
#  HackyStack Authentication
#  config/hackystack.php
# -----------------------------------------------------------------------------

HS_AUTH_FORM=

HS_AUTH_LOCAL_ENABLED=
HS_AUTH_LOCAL_2FA_ENABLED=
HS_AUTH_LOCAL_2FA_ENFORCED=
HS_AUTH_LOCAL_REGISTER_ENABLED=
HS_AUTH_LOCAL_RESET_ENABLED=
HS_AUTH_LOCAL_VERIFY_ENABLED=

HS_AUTH_LOCAL_PASSWORD_COUNT_DAYS_UNTIL_MUST_CHANGE=120
HS_AUTH_LOCAL_FAILED_LOGIN_ATTEMPT_LIMIT_MAX=8
HS_AUTH_LOCAL_FAILED_LOGIN_FORCE_RESET_PASSWORD=false
HS_AUTH_LOCAL_FAILED_LOGIN_FORCE_ACCOUNT_LOCK=true

# -----------------------------------------------------------------------------
#  HackyStack Cloud
#  config/hackystack.php
# -----------------------------------------------------------------------------

HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_METHOD=plus
HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_PREFIX=
HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_DOMAIN=

HS_CLOUD_ACCOUNTS_GCP_ENVIRONMENTS_DNS_PREFIX=
HS_CLOUD_ACCOUNTS_GCP_ENVIRONMENTS_DNS_BASE_DOMAIN=
HS_CLOUD_ACCOUNTS_GCP_ENVIRONMENTS_DNS_BASE_ZONE_NAME=

# -----------------------------------------------------------------------------
#  HackyStack Developer
#  config/hackystack.php
# -----------------------------------------------------------------------------

HS_DEVELOPER_GITLAB_API_USERNAME=
HS_DEVELOPER_GITLAB_API_TOKEN=

# -----------------------------------------------------------------------------
#  HackyStack Logging, Privacy, and Security
#  config/hackystack.php
# -----------------------------------------------------------------------------

HS_ANALYTICS_GOOGLE_ENABLED=false
HS_ANALYTICS_GOOGLE_TRACKING_ID=null
HS_LOG_IP_ADDRESS_ENABLED=true
HS_LOG_IP_ADDRESS_LOCATION=true
HS_LOG_USER_AGENT_ENABLED=true

# -----------------------------------------------------------------------------
#  HackyStack UI Settings
#  config/hackystack.php
# -----------------------------------------------------------------------------

HS_FOOTER_COPYRIGHT="Powered by HackyStack"
HS_FOOTER_DOCUMENTATION_LINK="https://docs.hackystack.io"
HS_FOOTER_SUPPORT_LINK="https://gitlab.com/gitlab-com/business-technology/engineering/tools/hackystack/-/issues"
HS_FOOTER_SUPPORT_INSTRUCTIONS=""

# -----------------------------------------------------------------------------
#  HackyStack Storage
#  config/hackystack.php
# -----------------------------------------------------------------------------

HS_STORAGE_CHOWN_USER="envoy"
HS_STORAGE_CHOWN_GROUP="www-data"

# -----------------------------------------------------------------------------
#  Glamstack SDK Configuration
#  config/glamstack-*
# -----------------------------------------------------------------------------

GITLAB_COM_BASE_URL="https://gitlab.com"
GITLAB_COM_ACCESS_TOKEN=""
GITLAB_GITOPS_BASE_URL="https://gitops.mycompany.com"
GITLAB_GITOPS_ACCESS_TOKEN=""

GOOGLE_DEFAULT_CONNECTION="gcp_org"

OKTA_DEFAULT_CONNECTION="dev"
OKTA_PROD_BASE_URL="https://mycompany.okta.com"
OKTA_PROD_API_TOKEN=""
OKTA_PREVIEW_BASE_URL="https://mycompany.oktapreview.com"
OKTA_PREVIEW_API_TOKEN=""
OKTA_DEV_BASE_URL="https://dev-12345678-admin.okta.com"
OKTA_DEV_API_TOKEN=""

# -----------------------------------------------------------------------------
#  Backend Application Configuration
# -----------------------------------------------------------------------------

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

# -----------------------------------------------------------------------------
# Pest PHP Testing Variables
# -----------------------------------------------------------------------------

TESTS_CLOUD_PROVIDER_ID=""
TESTS_IAM_EMAIL="user:dmurphy@example.com"
TESTS_GCP_PROJECT_ID="123456789012"
TESTS_GCP_PROJECT_NAME="my-project-a1b2c3"
