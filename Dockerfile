FROM registry.gitlab.com/hackystack/laravel-docker/laravel-docker:7.4

ENV APPDIR /opt/hackystack



COPY . ${APPDIR}
RUN rm -fr ${APPDIR}/tests ${APPDIR}/mysql ${APPDIR}/docker-compose.yml ${APPDIR}/Dockerfile ${APPDIR}/.git* ${APPDIR}/dev_env
# Create the env file.
# RUN mv ${APPDIR}/.env.example ${APPDIR}/.env

WORKDIR ${APPDIR}

# Authentication Groups
COPY ./config/hackystack-auth-groups.php.example ${APPDIR}/config/hackystack-auth-groups.php

# Authentication Provider Fields
COPY ./config/hackystack-auth-provider-fields.php.example ${APPDIR}/config/hackystack-auth-provider-fields.php

# Cloud Realms, Organization Units, and Accounts
COPY ./config/hackystack-cloud-realms.php.example ${APPDIR}/config/hackystack-cloud-realms.php
COPY .env.example .env
# Download Composer dependencies
RUN composer install

# Download npm depdendencies
RUN npm install

# Init out config file.
RUN php hacky hackystack:install -n
