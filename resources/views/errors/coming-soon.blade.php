@extends('errors._layouts.core')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="clearfix">
                    <h1 class="float-left display-3 mr-4" style="margin-bottom: 40px;"><i class="ri-git-branch-line"></i></h1>
                    <h4 class="pt-3">Iteration in progress</h4>
                    <p class="text-muted">You have found a placeholder for a feature that will be available in an upcoming release. Please check back soon.</p>
                    <div class="text-left">
                        <a class="btn btn-outline-primary" href="{{ route('user.dashboard.index') }}">Return to Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
