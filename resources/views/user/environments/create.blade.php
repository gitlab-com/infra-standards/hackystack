@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <!-- Page Header -->
            <div class="d-flex">
                <div class="flex-grow-1">
                    <h3>Create a Terraform Environment</h3>
                </div>
                <div class="">
                    <a class="btn btn-outline-dark mr-2" href="{{ route('user.environments.index') }}">Cancel and Go Back</a>
                </div>
            </div>
            <!-- END Page Header -->

            <div class="row mt-4">
                <div class="col-8">

                    <!-- Cloud Accounts -->
                    <div class="card">
                        <div class="card-body">
                            <form method="GET" action="{{ route('user.environments.create') }}">
                                <div class="form-group">
                                    <label class="font-weight-bold" for="cloud_provider_id">Cloud Account</label>
                                    @error('cloud_provider_id')
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ $message }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    @enderror
                                    @if($cloud_accounts->count() > 0)
                                        <select class="form-control" id="cloud_account_id" name="cloud_account_id" onchange="this.form.submit()">
                                            <option {!! $request->has('cloud_account_id') ? '' : 'selected="selected"' !!} disabled="disabled">Select...</option>
                                            @foreach($cloud_accounts as $cloud_account_option)
                                                @if($request->has('cloud_account_id') && $request->cloud_account_id == $cloud_account_option->id)
                                                    <option selected="selected" value="{{ $cloud_account_option->id }}">
                                                        {{ '['.$cloud_account_option->cloudProvider->type.'] '.$cloud_account_option->slug.'-'.$cloud_account_option->short_id }}
                                                    </option>
                                                @else
                                                    <option value="{{ $cloud_account_option->id }}">
                                                        {{ '['.$cloud_account_option->cloudProvider->type.'] '.$cloud_account_option->slug.'-'.$cloud_account_option->short_id }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                            You have not created any cloud accounts that are eligible for Terraform environments.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Cloud Accounts -->

                    @if($cloud_account != null)

                        @if($environment_templates->count() == 0)

                            <!-- Cloud Environment Template -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="font-weight-bold" for="cloud_provider_id">Environment Template</label>
                                        <div class="alert alert-primary fade show" role="alert">
                                            No environment templates are available for this Cloud Provider ({{ $cloud_account->cloudProvider->type }}).
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Environment Template -->

                        @elseif($gcp_service_usage_api_enabled == false)

                            <div class="alert alert-warning fade show" role="alert">
                                The GCP Service Usage API does not appear to be enabled for this GCP project. This needs to be enabled using the GCP Console before you can create a new Environment.<br />
                                <br />
                                <a class="btn btn-warning" target="_blank" href="https://console.cloud.google.com/apis/api/serviceusage.googleapis.com/metrics?project={{ $cloud_account->api_meta_data['projectId'] }}">Open GCP Console</a><br />
                                <br />
                                Please refresh this page after you have enabled the Service Usage API. Please try again in a few moments if the state has not updated yet.
                            </div>

                        @elseif(in_array(false, $gcp_apis_enabled))

                            @foreach($gcp_apis_enabled as $service => $status)

                                @if($status == false)

                                    <div class="alert alert-warning fade show mb-4" role="alert">
                                        The GCP {{ Str::title($service) }} API does not appear to be enabled for this GCP project. This needs to be enabled using the GCP Console before you can create a new Environment.<br />
                                        <br />
                                        <a class="btn btn-warning" target="_blank" href="https://console.cloud.google.com/apis/api/{{ $service }}.googleapis.com/metrics?project={{ $cloud_account->api_meta_data['projectId'] }}">Open GCP Console</a><br />
                                        <br />
                                        Please refresh this page after you have enabled the API. Please try again in a few moments if the state has not updated yet. You can monitor the progress in the GCP console in the top right corner in the notifications drop down.
                                    </div>

                                @endif

                            @endforeach

                        @else

                        <div class="alert alert-success fade show mb-4" role="alert">
                            The GCP APIs appear to be enabled successfully for this project. You may need to enable additional APIs to use other GCP services with Terraform resources and modules that you add.<br />
                            <ul class="mt-2">
                                @foreach($gcp_apis_enabled as $service => $status)
                                    <li><code>{{ $service }}.googleapis.com</code></li>
                                @endforeach
                            </ul>
                        </div>

                            <form id="environment" method="POST" action="{{ route('user.environments.store') }}">
                                @csrf
                                <input type="hidden" name="cloud_account_id" value="{{ $cloud_account->id }}">

                                <!-- Cloud Environment Template -->
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="font-weight-bold" for="cloud_provider_id">Environment Template</label>
                                            @error('cloud_account_environment_template_id')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                {{ $message }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            @enderror
                                            <select class="form-control" id="cloud_account_environment_template_id" name="cloud_account_environment_template_id">
                                                <option {!! $request->has('cloud_account_environment_template_id') ? '' : 'selected="selected"' !!} disabled="disabled">Select...</option>
                                                @foreach($environment_templates as $template)
                                                    @if($request->has('cloud_account_environment_template_id') && $request->cloud_account_environment_template_id == $template->id)
                                                        <option selected="selected" value="{{ $template->id }}">
                                                            {{ $template->rendered_name.'-'.$template->short_id }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $template->id }}">
                                                            {{ $template->rendered_name.'-'.$template->short_id }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Environment Template -->

                                <!-- Environment Name -->
                                <div class="card">
                                    <div class="card-body">

                                        <div class="form-group">
                                            <label class="font-weight-bold" for="account_name">Environment Name (Alphadash Slug)</label>
                                            @error('name')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                {{ $message }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            @enderror
                                            <input type="text" class="form-control" id="name" name="name" style="width: 320px;" maxlength="30">
                                        </div>

                                    </div>
                                </div>
                                <!-- END Environment Name -->

                                <!-- Form Action Buttons -->
                                <div class="card">
                                    <div class="card-body text-right" x-data="{ buttonDisabled: false, title: 'Create Environment', cursor: 'pointer' }">
                                        <a x-show="!buttonDisabled" class="btn btn-outline-dark mr-2" href="{{ route('user.environments.index') }}">Cancel and Go Back</a>
                                        <button x-on:click="buttonDisabled = true; title = 'Provisioning 🚀'; cursor = 'wait'; document.getElementById('environment').submit();" x-bind:disabled="buttonDisabled" x-bind:style="{ 'cursor': cursor }" type="submit" class="btn btn-success" x-text="title"></button>
                                    </div>
                                </div>
                                <!-- Form Action Buttons -->

                            </form>

                        @endif
                    @endif

                </div>
                <div class="col-4">

                    <!-- Documentation -->
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">Documentation</h5>
                        </div>
                        <div class="card-body">
                            <p>
                                When creating a Terraform Environment, you need to select which Cloud Account the environment will be provisioned for.<br />
                                <br />
                                You can select one of the available Environment Template(s) that have been configured by your system administrator. After the Terraform Environment has been provisioned, you can customize the Git project as needed.<br />
                                <br />
                                After clicking the <strong>Create Environment</strong> button, please use the provided Git credentials and navigate to the Git repository to start customizing your Terraform environment configuration.
                            </p>
                        </div>
                    </div>
                    <!-- END Documentation -->

                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
