<?php

namespace App\Console\Commands;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class HackystackInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hackystack:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform initial installation of HackyStack with auth tenant, groups, roles, and cloud providers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        system('clear');
        $this->line('<fg=cyan>  _   _               _            ____   _                _     </>');
        $this->line('<fg=cyan> | | | |  __ _   ___ | | __ _   _ / ___| | |_  __ _   ___ | | __ </>');
        $this->line('<fg=cyan> | |_| | / _` | / __|| |/ /| | | |\___ \ | __|/ _` | / __|| |/ / </>');
        $this->line('<fg=cyan> |  _  || (_| || (__ |   < | |_| | ___) || |_| (_| || (__ |   <  </>');
        $this->line('<fg=cyan> |_| |_| \__,_| \___||_|\_\ \__, ||____/  \__|\__,_| \___||_|\_\ </>');
        $this->line('<fg=cyan>                            |___/                                </>');
        $this->line('');

        if(config('hackystack.installed') == true) {
            $this->line('Error: HackyStack has already been installed.');
            $this->line('Please run `php hacky hackystack:reset` to remove the existing configuration.');
            $this->error('');
            die();
        } else {
            $this->line('Thank you for downloading HackyStack! Let\'s get started...');
        }

        $this->line('');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=cyan> Initializing configuration files                                              </>');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('');

        // Validate that .env file exists
        if(!is_file(base_path('.env'))) {
            $this->line('<fg=green>Copying environment configuration template `.env.example` to `.env`</>');
            File::copy(base_path('.env.example'), base_path('.env'));
        } else {
            $this->line('<fg=yellow>Environment configuration exists at `.env`</>');
        }

        // Application encryption key
        if(config('app.key') == null) {
            $this->comment('<fg=green>Generating application encryption key</>');
            Artisan::call('key:generate');
        } else {
            $this->line('<fg=yellow>Application encryption key already configured</>');
        }

        // Clear configuration cache
        $this->call('cache:clear', []);
        $this->call('config:clear', []);
        $this->line('');

        if(config('app.url') == 'http://localhost') {

            // Application URL
            $this->line('Local URL examples:');
            $this->line('http://localhost:8080');
            $this->line('http://hackystack-portal.valet');
            $this->line('');
            $this->line('DNS URL examples:');
            $this->line('https://hackystack.mycompany.com');
            $this->line('https://infra-portal.mycompany.net');
            $this->line('');
            $this->line('Current Value: '.config('app.url'));
            $this->line('');
            $app_url = $this->anticipate('What is the base URL where this instance will be running?', [config('app.url')]);

            // Update .env file with application URL
            $env_file_content = file_get_contents(base_path('.env'));
            $updated_file_content = preg_replace("/^APP_URL=$/m", "APP_URL=".$app_url, $env_file_content);
            file_put_contents(base_path('.env'), $updated_file_content);

            // Local or production
            if($this->confirm('Will this application be running in production on this machine?')) {

                // Update .env file with production values
                $env_file_content = file_get_contents(base_path('.env'));
                $updated_file_content = preg_replace("/^APP_ENV=local$/m", "APP_URL=production", $env_file_content);
                file_put_contents(base_path('.env'), $updated_file_content);

                // Update .env file with production values
                $env_file_content = file_get_contents(base_path('.env'));
                $updated_file_content = preg_replace("/^APP_DEBUG=true$/m", "APP_DEBUG=false", $env_file_content);
                file_put_contents(base_path('.env'), $updated_file_content);

            }

        } else {
            $this->line('<fg=yellow>Application URL set to '.config('app.url'));
        }

        // Clear configuration cache
        $this->call('cache:clear', []);
        $this->call('config:clear', []);

        $this->line('');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=cyan> Configuring MySQL database                                                    </>');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('');

        if(config('database.connections.mysql.host') == null) {

            if($this->confirm('Will you be running the MySQL database on this machine?')) {
                $database_host = '127.0.0.1';
                $this->line('Database Host: '.$database_host);
            } else {
                $database_host = $this->anticipate('What is the IP address or FQDN of your database server?', ['127.0.0.1']);
            }

            // Update .env file with database credentials
            $env_file_content = file_get_contents(base_path('.env'));
            $updated_file_content = preg_replace("/^DB_HOST=$/m", "DB_HOST=".$database_host, $env_file_content);
            file_put_contents(base_path('.env'), $updated_file_content);

        } else {
            $database_host = config('database.connections.mysql.host');
            $this->line('Database Host: '.$database_host);
            $this->line('');
        }

        if(config('database.connections.mysql.database') == null) {
            $database_name = $this->ask('What is the name of the database? (Example `hackystack_db`)');

            // Update .env file with database credentials
            $env_file_content = file_get_contents(base_path('.env'));
            $updated_file_content = preg_replace("/^DB_DATABASE=$/m", "DB_DATABASE=".$database_name, $env_file_content);
            file_put_contents(base_path('.env'), $updated_file_content);

        } else {
            $database_name = config('database.connections.mysql.database');
            $this->line('Database Name: '.$database_name);
            $this->line('');
        }

        if(config('database.connections.mysql.username') == null) {
            $this->line('<fg=red>For security reasons, do not use the `root` user account. You</>');
            $this->line('<fg=red>should create a user and grant permissions to the database.</>');
            $database_username = $this->ask('What is the username for connecting to your database server? ');

            // Update .env file with database credentials
            $env_file_content = file_get_contents(base_path('.env'));
            $updated_file_content = preg_replace("/^DB_USERNAME=$/m", "DB_USERNAME=".$database_username, $env_file_content);
            file_put_contents(base_path('.env'), $updated_file_content);

        } else {
            $database_username = config('database.connections.mysql.username');
            $this->line('Database Username: '.$database_username);
            $this->line('');
        }

        if(config('database.connections.mysql.password') == null) {
            $database_password = $this->ask('What is the password for connecting to your database server?');

            // Update .env file with database credentials
            $env_file_content = file_get_contents(base_path('.env'));
            $updated_file_content = preg_replace("/^DB_PASSWORD=$/m", "DB_PASSWORD=".$database_password, $env_file_content);
            file_put_contents(base_path('.env'), $updated_file_content);

        } else {
            $database_password = config('database.connections.mysql.password');
            $this->line('Database Password: (hidden for security reasons)');
            $this->line('');
        }

        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=cyan> Configuring authentication providers                                          </>');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('');

        if(env('HS_AUTH_LOCAL_ENABLED') == null) {

            if($this->confirm('Do you want users to be able to sign in with local authentication (non-SSO)?')) {
                $auth_local_enabled = true;
            } else {
                $auth_local_enabled = false;
            }

            // Update .env file with auth_local_enabled value
            $env_file_content = file_get_contents(base_path('.env'));
            $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_ENABLED=$/m", "HS_AUTH_LOCAL_ENABLED=".$auth_local_enabled, $env_file_content);
            file_put_contents(base_path('.env'), $updated_file_content);

            if($auth_local_enabled == true) {

                // Update .env file with authentication form
                if(env('HS_AUTH_FORM') == null) {
                    $env_file_content = file_get_contents(base_path('.env'));
                    $updated_file_content = preg_replace("/^HS_AUTH_FORM=$/m", "HS_AUTH_FORM=local", $env_file_content);
                    file_put_contents(base_path('.env'), $updated_file_content);
                }

                // Self Registration
                if($this->confirm('Do you want users to be able to self register with local authentication?')) {
                    $env_file_content = file_get_contents(base_path('.env'));
                    $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_REGISTER_ENABLED=true", $env_file_content);
                    file_put_contents(base_path('.env'), $updated_file_content);
                } else {
                    $env_file_content = file_get_contents(base_path('.env'));
                    $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_REGISTER_ENABLED=false", $env_file_content);
                    file_put_contents(base_path('.env'), $updated_file_content);
                }

                // Email Verification
                if($this->confirm('Do you want to allow users to sign in without verifying their email address?')) {
                    $env_file_content = file_get_contents(base_path('.env'));
                    $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_VERIFY_ENABLED=false", $env_file_content);
                    file_put_contents(base_path('.env'), $updated_file_content);
                } else {
                    $env_file_content = file_get_contents(base_path('.env'));
                    $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_VERIFY_ENABLED=true", $env_file_content);
                    file_put_contents(base_path('.env'), $updated_file_content);
                }

                // Two Factor Autentication
                if($this->confirm('Do you want to enable two factor authentication (2FA)?')) {
                    $env_file_content = file_get_contents(base_path('.env'));
                    $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_2FA_ENABLED=true", $env_file_content);
                    file_put_contents(base_path('.env'), $updated_file_content);

                    if($this->confirm('Do you want to require two factor authentication (2FA) for all users?')) {
                        $env_file_content = file_get_contents(base_path('.env'));
                        $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_2FA_ENFORCED=true", $env_file_content);
                        file_put_contents(base_path('.env'), $updated_file_content);
                    } else {
                        $env_file_content = file_get_contents(base_path('.env'));
                        $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_2FA_ENFORCED=false", $env_file_content);
                        file_put_contents(base_path('.env'), $updated_file_content);
                    }

                } else {
                    $env_file_content = file_get_contents(base_path('.env'));
                    $updated_file_content = preg_replace("/^HS_AUTH_LOCAL_REGISTER_ENABLED=$/m", "HS_AUTH_LOCAL_2FA_ENABLED=false", $env_file_content);
                    file_put_contents(base_path('.env'), $updated_file_content);
                }

            } elseif($auth_local_enabled = false) {
                $env_file_content = file_get_contents(base_path('.env'));
                $updated_file_content = preg_replace("/^HS_AUTH_FORM=$/m", "HS_AUTH_FORM=providers", $env_file_content);
                file_put_contents(base_path('.env'), $updated_file_content);
            }

        }

        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=cyan> Configuring AWS cloud provider                                                </>');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('');

        if(env('HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_PREFIX') == null) {

            if($this->confirm('Will you be using AWS as a cloud provider?')) {

                $this->line('Each Cloud Account requires a globally unique root email address');
                $this->line('HackyStack uses the + alias method to create email addresses for each Cloud Account.');
                $this->line('Example: <fg=cyan>hackystack-admins+a1b2c3d4@mycompany.com</>');
                $this->line('');
                $this->line('We recommend that you create a email group or shared account for root accounts.');
                $this->line('If you do not have an email group created yet, you can use your own email address');
                $this->line('for the initial configuration and change this in the `.env` file later.');

                $root_email_plus_prefix = $this->ask('What is the email address handle you want to use (Example: hackystack-admins)?');

                // Update .env file with auth_local_enabled value
                $env_file_content = file_get_contents(base_path('.env'));
                $updated_file_content = preg_replace("/^HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_PREFIX=$/m", "HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_PREFIX=".$root_email_plus_prefix, $env_file_content);
                file_put_contents(base_path('.env'), $updated_file_content);

                $root_email_plus_domain = $this->ask('What is the email address domain (without the @ symbol) you want to use (Example: mycompany.com)?');

                // Update .env file with auth_local_enabled value
                $env_file_content = file_get_contents(base_path('.env'));
                $updated_file_content = preg_replace("/^HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_DOMAIN=$/m", "HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_DOMAIN=".$root_email_plus_domain, $env_file_content);
                file_put_contents(base_path('.env'), $updated_file_content);

            } else {
                //
            }

        }

        // Clear configuration cache
        // Call the get method to display the tables of values for the record.
        $this->call('cache:clear', []);
        $this->call('config:clear', []);

        // Confirmation message
        $this->line('');
        $this->line('<fg=cyan>-------------------------------------------------------------------------------</>');
        $this->line('<fg=green;options=bold>HackyStack initial installation complete.</>');
        $this->line('');
        $this->line('<fg=cyan;options=bold>Next Steps:</>');
        $this->line('<fg=cyan>`php hacky hackystack:install-database` to run database migrations and seed with system defaults.</>');
        $this->line('<fg=cyan>`php hacky auth-provider:list` to get a list of SSO providers you can configure.</>');
        $this->line('<fg=cyan>`php hacky auth-provider:edit a1b2c3d4` to configure one or more SSO providers.</>');
        $this->line('<fg=cyan>`php hacky auth-user:create` to create your first user account.</>');
        $this->line('<fg=cyan>`php hacky auth-group-user:create --auth_group_slug=auth-super-admin` to attach your user to the administrators group.</>');
        $this->line('<fg=cyan>`php hacky auth-group:list` to see a list of all available groups that other users can be attached to.</>');
        $this->line('');
        $this->line('You can run `php hacky` to see a list of commands to administer HackyStack.');
        $this->line('');

    }
}
