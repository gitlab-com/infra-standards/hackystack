<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthUserGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:get
                            {short_id_email? : The short ID or email addresses of the user.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication User by ID or Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id_email = $this->argument('short_id_email');

        // If short ID or slug is not set, return an error message
        if ($short_id_email == null) {
            $this->error('You did not specify the short id or email to lookup the record.');
            $this->line('You can get a list of users using `auth-user:list`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif ($short_id_email != null) {
            $auth_user = Models\Auth\AuthUser::query()
                ->where('short_id', $short_id_email)
                ->orWhere('email', $short_id_email)
                ->first();
        }

        // If record not found, return an error message
        if ($auth_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Group Values
        $this->comment('');
        $this->comment('Auth User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_user->id
                ],
                [
                    'short_id',
                    $auth_user->short_id
                ],
                [
                    '[relationship] authTenant',
                    '[' . $auth_user->authTenant->short_id . '] ' . $auth_user->authTenant->slug
                ],
                [
                    '[relationship] authProvider',
                    '[' . $auth_user->authProvider->short_id . '] ' . $auth_user->authProvider->slug
                ],
                [
                    'full_name',
                    $auth_user->full_name
                ],
                [
                    'job_title',
                    $auth_user->job_title
                ],
                [
                    'organization_name',
                    $auth_user->organization_name
                ],
                [
                    'email',
                    $auth_user->email
                ],
                [
                    'email_recovery',
                    $auth_user->email_recovery
                ],
                [
                    'count_current_failed_logins',
                    $auth_user->count_current_failed_logins
                ],
                [
                    'flag_passwordless',
                    $auth_user->flag_passwordless
                ],
                [
                    'flag_2fa_enabled',
                    $auth_user->flag_2fa_enabled
                ],
                [
                    'flag_must_change_password',
                    $auth_user->flag_must_change_password
                ],
                [
                    'flag_account_expired',
                    $auth_user->flag_account_expired
                ],
                [
                    'flag_account_locked',
                    $auth_user->flag_account_locked
                ],
                [
                    'flag_account_verified',
                    $auth_user->flag_account_verified
                ],
                [
                    'flag_terms_accepted',
                    $auth_user->flag_terms_accepted
                ],
                [
                    'flag_privacy_accepted',
                    $auth_user->flag_privacy_accepted
                ],
                [
                    'expires_at',
                    $auth_user->expires_at ? $auth_user->expires_at->toIso8601String() : ''
                ],
                [
                    'locked_at',
                    $auth_user->locked_at ? $auth_user->locked_at->toIso8601String() : ''
                ],
                [
                    'verified_at',
                    $auth_user->verified_at ? $auth_user->verified_at->toIso8601String() : ''
                ],
                [
                    'locked_at',
                    $auth_user->locked_at ? $auth_user->locked_at->toIso8601String() : ''
                ],
                [
                    'terms_accepted_at',
                    $auth_user->terms_accepted_at ? $auth_user->terms_accepted_at->toIso8601String() : ''
                ],
                [
                    'privacy_accepted_at',
                    $auth_user->privacy_accepted_at ? $auth_user->privacy_accepted_at->toIso8601String() : ''
                ],
                [
                    'password_changed_at',
                    $auth_user->password_changed_at ? $auth_user->password_changed_at->toIso8601String() : ''
                ],
                [
                    'last_successful_login_at',
                    $auth_user->last_successful_login_at ? $auth_user->last_successful_login_at->toIso8601String() : ''
                ],
                [
                    'last_failed_login_at',
                    $auth_user->last_failed_login_at ? $auth_user->last_failed_login_at->toIso8601String() : ''
                ],
                [
                    'last_activity_at',
                    $auth_user->last_activity_at ? $auth_user->last_activity_at->toIso8601String() : ''
                ],
                [
                    'created_at',
                    $auth_user->created_at ? $auth_user->created_at->toIso8601String() : ''
                ],
                [
                    'updated_at',
                    $auth_user->updated_at ? $auth_user->updated_at->toIso8601String() : ''
                ],
                [
                    'deleted_at',
                    $auth_user->deleted_at ? $auth_user->deleted_at->toIso8601String() : ''
                ],
                [
                    'state',
                    $auth_user->state
                ],
            ]
        );

        $provider_meta_data_values = [];
        foreach ($auth_user->provider_meta_data as $meta_data_key => $meta_data_value) {
            $provider_meta_data_values[] = [
                $meta_data_key,
                is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value
            ];
        }

        $this->table(
            ['Auth Provider Meta Data Column', 'API Value'],
            $provider_meta_data_values
        );

        // Check if option passed to not show child relationships
        if ($this->option('without-child-relationships') == false) {

            // Call the list methods to display the tables of values for the record.

            $this->call('auth-group-user:list', [
                '--auth_user_short_id' => $auth_user->short_id
            ]);
        }

        // Get list of records from database
        $records = Models\Cloud\CloudAccountUser::query()
            ->with([
                'authTenant',
                'cloudProvider',
                'cloudAccount',
                'cloudAccount.cloudOrganizationUnit'
            ])->withCount([
                'cloudAccountUserRoles'
            ])->whereHas('cloudAccount')
            ->where('auth_user_id', $auth_user->id)
            ->orderBy('created_at')
            ->get();

        // Loop through groups in Eloquent model and add values to array
        foreach ($records as $record) {
            $table_rows[] = [
                'short_id' => $record->short_id,
                'cloud_provider' => '[' . $record->cloudProvider->short_id . '] ' . $record->cloudProvider->slug,
                'cloud_organization_unit' => '[' . $record->cloudAccount->cloudOrganizationUnit->short_id . '] ' . $record->cloudAccount->cloudOrganizationUnit->slug,
                'cloud_account' => '[' . $record->cloudAccount->short_id . '] ' . $record->cloudAccount->slug,
                'cloud_account_user_roles' => $record->cloud_account_user_roles_count,
                'username' => $record->username,
                'created_at' => $record->created_at->toIso8601String(),
                'provisioned_at' => $record->provisioned_at ? $record->provisioned_at->toIso8601String() : 'not provisioned',
                'state' => $record->state,
            ];
        }

        $this->comment('Cloud Account User Relationships - List of Records');

        if (count($records) > 0) {

            // Show table in console output for verification
            $table_headers = ['Short ID', 'Provider', 'Organization Unit', 'Cloud Account', 'Roles', 'IAM Username', 'Created at', 'Provisioned at', 'State'];
            $this->table($table_headers, $table_rows);

            $this->comment('Total: ' . count($records));
            $this->comment('');
        } else {
            $this->line('No records exist.');
            $this->line('You can attach cloud accounts to users using `cloud-account-user:create`.');
            $this->comment('');
        }

        $this->comment('');
    }
}
