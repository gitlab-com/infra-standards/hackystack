<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthUserCheckIfGitUserExists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:check-if-git-user-exists
                            {short_id? : The short ID of the user.}
                            {--provision : If the Git user does not exist, generate credentials if not already created and provision the Git user}
                            {--deprovision : If the Git user does exist, deprovision the Git user}
                            {--with-password : The Git password will be revealed in the output}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if Git User credentials exist and has been provisioned.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-user:check-if-git-user-exists a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_user = Models\Auth\AuthUser::with([
                    'authTenant'
                ])
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Flags
        $provision = $this->option('provision');
        $deprovision = $this->option('deprovision');
        $with_password = $this->option('with-password');

        // Initialize service
        $authUserService = new Services\V1\Auth\AuthUserService();

        // Auth User Values
        $this->comment('');
        $this->comment('Auth User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'short_id',
                    $auth_user->short_id
                ],
                [
                    'auth_tenant',
                    '['.$auth_user->authTenant->short_id.'] '.$auth_user->authTenant->slug,
                ],
                [
                    'full_name',
                    $auth_user->full_name
                ],
                [
                    'email',
                    $auth_user->email
                ],
                [
                    'git_user_email',
                    $auth_user->user_handle.'+'.Str::slug(config('hackystack.app.name')).'-gitops-'.$auth_user->short_id.'@'.$auth_user->user_email_domain
                ],
                [
                    'git_provider_base_url',
                    $auth_user->authTenant->git_provider_base_url != null ? '<fg=green>'.$auth_user->authTenant->git_provider_base_url.'</>' : '<fg=red>Not configured yet. Use `auth-tenant:edit '.$auth_user->authTenant->short_id.'`</>'
                ],
                [
                    'git_username',
                    $auth_user->git_username ? '<fg=green>'.$auth_user->git_username.'</>' : '<fg=red>Not generated yet. Use `auth-user:provision-git-user '.$auth_user->short_id.'`</>'
                ],
                [
                    'git_password',
                    $auth_user->git_password ? ($with_password ? '<fg=cyan>'.decrypt($auth_user->git_password).'</>' : '<fg=yellow>Masked. Use --with-password flag to reveal.</>') : '<fg=red>Not generated yet. Use `auth-user:provision-git-user '.$auth_user->short_id.'`</>'
                ],
                [
                    'git_meta_data',
                    $auth_user->git_meta_data ? Str::limit(json_encode($auth_user->git_meta_data), 80, '...') : '<fg=red>Not provisioned yet. Use `auth-user:provision-git-user '.$auth_user->short_id.'`</>'
                ],
                [
                    'flag_git_user_provisioned',
                    $auth_user->flag_git_user_provisioned ? '<fg=green>true</>' : '<fg=red>false</>'
                ],
            ]
        );

        $this->newLine();

        if($provision == true && $auth_user->flag_git_user_provisioned == 0) {
            if($this->confirm('Do you want to proceed with provisioning this Git user?')) {
                $this->call('auth-user:provision-git-user', [
                    'short_id' => $auth_user->short_id
                ]);
            }
        }

        if($deprovision == true && $auth_user->flag_git_user_provisioned == 1) {
            if($this->confirm('<bg=red;fg=white;bold>Do you want to proceed with deprovisioning this Git user?</>')) {
                $this->call('auth-user:deprovision-git-user', [
                    'short_id' => $auth_user->short_id
                ]);
            }
        }

    }
}
