<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthProviderGroupGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-group:get
                            {short_id? : The short ID of the provider group.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication Provider Group by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short id or slug to lookup the record.');
            $this->line('You can get a list of provider groups using `auth-provider-group:get`');
            $this->line('You can get the record using the short ID using `auth-provider-group:delete a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_provider_group = Models\Auth\AuthProviderGroup::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_provider_group == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Provider Group Values
        $this->comment('');
        $this->comment('Auth Provider Group');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_provider_group->id
                ],
                [
                    'short_id',
                    $auth_provider_group->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$auth_provider_group->authTenant->short_id.'] '.$auth_provider_group->authTenant->slug
                ],
                [
                    '[relationship] authProvider',
                    '['.$auth_provider_group->authProvider->short_id.'] '.$auth_provider_group->authProvider->slug
                ],
                [
                    '[relationship] authGroup',
                    '['.$auth_provider_group->authGroup->short_id.'] '.$auth_provider_group->authGroup->slug
                ],
                [
                    'type',
                    $auth_provider_group->type
                ],
                [
                    'meta_key',
                    $auth_provider_group->meta_key
                ],
                [
                    'meta_value',
                    $auth_provider_group->meta_value
                ],
                [
                    'created_at',
                    $auth_provider_group->created_at->toIso8601String()
                ],
            ]
        );

        $this->comment('');

    }
}
