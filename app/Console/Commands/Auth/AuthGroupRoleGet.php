<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthGroupRoleGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group-role:get
                            {short_id? : The short ID of the group role.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication Group Role by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-group-role:get a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_group_role = Models\Auth\AuthGroupRole::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_group_role == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Group Values
        $this->comment('');
        $this->comment('Auth Group Role');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_group_role->id
                ],
                [
                    'short_id',
                    $auth_group_role->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$auth_group_role->authTenant->short_id.'] '.$auth_group_role->authTenant->slug
                ],
                [
                    '[relationship] authGroup',
                    '['.$auth_group_role->authGroup->short_id.'] '.$auth_group_role->authGroup->slug
                ],
                [
                    '[relationship] authRole',
                    '['.$auth_group_role->authRole->short_id.'] '.$auth_group_role->authRole->slug
                ],
                [
                    'created_at',
                    $auth_group_role->created_at->toIso8601String()
                ],
                [
                    'expires_at',
                    $auth_group_role->expires_at ? $auth_group_role->expires_at->toIso8601String() : 'never'
                ],
            ]
        );

        $this->comment('');

    }
}
