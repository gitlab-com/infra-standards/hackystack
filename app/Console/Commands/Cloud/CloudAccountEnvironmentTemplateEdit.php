<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use App\Services\V1\Vendor\Gcp\CloudBillingOrganizationService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudAccountEnvironmentTemplateEdit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment-template:edit
                            {short_id? : The short ID of the environment template.}
                            {--name= : The new display name of this environment template}
                            {--git_name : If set, the custom display name will be removed and the Git project name will be used.}
                            {--description= : The new display description of this environment template}
                            {--git_description : If set, the custom display name will be removed and the Git project name will be used.}
                            {--state= : Update the state of the environment template (active|inactive|archived)}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update a Cloud Account Environment Template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Account Environment Template - Edit Record');

        // Get arguments and options
        $short_id = $this->argument('short_id');
        $name = $this->option('name');
        $description = $this->option('description');
        $state = $this->option('state');

        // If short ID was specified, lookup by ID
        if($short_id) {
            $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If short ID or slug is not set, return an error message
        else {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of environment templates using `cloud-account-environment-template:list`');
            $this->line('You can lookup by short ID using `cloud-account-environment-template:get a1b2c3d4`');
            $this->line('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Name
        $name = $this->option('name');

        // If git_name flag is set, set description to null value.
        if($this->option('git_name')) {
            $name = null;
            $this->line('<fg=yellow>The --git_name flag has been set, so the existing name value will be set to null to use the Git name.');
        }

        // If git_name flag is not set
        elseif($name == null) {
            if($this->confirm('The current name of this environment template is `'.$cloud_account_environment_template->name.'`. Do you want to change the name?')) {
                // Prompt the user for the new name
                $name = $this->ask('What is the new display name of this environment template?');
            } else {
                // Use the current name
                $name = $cloud_account_environment_template->name;
            }
        }

        // Description
        $description = $this->option('description');
        $git_description = $this->option('git_description');

        // If git_description flag is set, set description to null value.
        if($this->option('git_description')) {
            $description = null;
            $this->line('<fg=yellow>The --git_description flag has been set, so the existing description value will be set to null to use the Git description.');
        }

        // If git_description flag is not set
        elseif($description == null) {
            if($this->confirm('The current description of this environment template is `'.Str::limit($cloud_account_environment_template->description, 50, '...').'`. Do you want to change the description?')) {
                // Prompt the user for the new description
                $description = $this->ask('What is the new description for this environment template?');
            } else {
                // Use the current description
                $description = $cloud_account_environment_template->description;
            }
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            $this->comment('Cloud Account Environment Template');

            // Verify inputs table
            $this->table(
                ['Column', 'Old Value', 'New Value'],
                [
                    [
                        'short_id',
                        $cloud_account_environment_template->short_id,
                        $cloud_account_environment_template->short_id,
                    ],
                    [
                        'name',
                        $cloud_account_environment_template->name,
                        $name
                    ],
                    [
                        'description',
                        $cloud_account_environment_template->description,
                        $description
                    ],
                    [
                        'state',
                        $cloud_account_environment_template->state,
                        ($cloud_account_environment_template->state != $state ? '<fg=yellow>' : '<fg=white>').$state.'</>'
                    ],
                ]
            );

            // Ask for confirmation to abort update.
            if($this->confirm('Do you want to abort the update of the record?')) {
                $this->error('Error: You aborted. The record was not updated.');
                die();
            }

        }

        // Initialize service
        $cloudAccountEnvironmentTemplateService = new Services\V1\Cloud\CloudAccountEnvironmentTemplateService();

        // Use service to update record
        $record = $cloudAccountEnvironmentTemplateService->update($cloud_account_environment_template->id, [
            'name' => $name,
            'description' => $description,
            'state' => $state,
        ]);

        // Show result in console
        $this->comment('Record updated successfully.');

    }

}
