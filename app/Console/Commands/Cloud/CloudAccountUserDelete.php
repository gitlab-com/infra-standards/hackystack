<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountUserDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-user:delete
                            {short_id? : The short ID of the cloud account user.}
                            {--G|cloud_account_short_id= : The short ID of the Cloud Account}
                            {--U|auth_user_short_id= : The short ID of the Authentication User}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Cloud Account User by ID or relationship IDs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get cloud account and user short ID
        $cloud_account_short_id = $this->option('cloud_account_short_id');
        $auth_user_short_id = $this->option('auth_user_short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $cloud_account_short_id == null) {
            $this->error('You did not specify the cloud account user short_id or cloud_account_short_id to lookup the record.');
            $this->line('You can get a list of cloud accounts users using `cloud-account-user:list`');
            $this->line('You can get a list of cloud accounts using `cloud-account:list`');
            $this->line('You can get a list of authentication users using `auth-user:list`');
            $this->line('You can delete by short ID using `cloud-account-user:delete a1b2c3d4`');
            $this->line('You can delete by relationship IDs using `cloud-account-user:delete --cloud_account_short_id=a1b2c3d4 --auth_user_short_id=e5f6g7h8`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account_user = Models\Cloud\CloudAccountUser::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($cloud_account_short_id != null) {

            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('short_id', $cloud_account_short_id)
                ->first();

            // If record not found, return an error message
            if($cloud_account == null) {
                $this->error('No cloud account was found with that short ID.');
                $this->error('');
                die();
            }

            $auth_user = Models\Auth\AuthUser::query()
                ->where('short_id', $auth_user_short_id)
                ->first();

            // If record not found, return an error message
            if($auth_user == null) {
                $this->error('No user was found with that short ID.');
                $this->error('');
                die();
            }

            $cloud_account_user = Models\Cloud\CloudAccountUser::query()
                ->where('cloud_account_id', $cloud_account->id)
                ->where('auth_user_id', $auth_user->id)
                ->first();

        }

        // If record not found, return an error message
        if($cloud_account_user == null) {
            $this->error('No cloud account user relationship record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-account-user:get', [
            'short_id' => $cloud_account_user->short_id,
        ]);

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $cloudAccountUserService = new Services\V1\Cloud\CloudAccountUserService();

        // Use service to delete record
        $cloudAccountUserService->destroy($cloud_account_user->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
