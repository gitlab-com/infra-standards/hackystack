<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class CloudProviderCredential extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-provider:credential
                            {short_id? : The short ID of the cloud provider.}
                            {--T|auth_tenant_slug= : The slug of the tenant that provider belongs to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider that the credentials are used to access}
                            {--aws_access_key_id= : The access key ID (API username) of the AWS IAM account}
                            {--aws_access_key_secret= : The secret key (API password) of the AWS IAM account}
                            {--gcp_credentials_file= : The filename of the JSON file that contains the Google service account credentials in the storage/keys directory.}
                            {--parent_org_unit= : The AWS organizational unit ID (ou-a1b2-c3d4e5f) or GCP folder number (123456789012) that should be the parent for all realm organization unit folders. If not set, realm folders will be created in the top-level organization.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the credentials used for accessing the cloud provider API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Provider - Set credentials for accessing API');

        // Get arguments and options
        $short_id = $this->argument('short_id');
        $auth_tenant_slug = $this->option('auth_tenant_slug');
        $cloud_provider_slug = $this->option('cloud_provider_slug');
        $aws_access_key_id = $this->option('aws_access_key_id');
        $aws_access_key_secret = $this->option('aws_access_key_secret');
        $gcp_credentials_file = $this->option('gcp_credentials_file');
        $parent_org_unit = $this->option('parent_org_unit');

        // If short ID was specified, lookup by ID
        if($short_id) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug option was specified, lookup by slug
        elseif($auth_tenant_slug && $cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->whereHas('authTenant', function(Builder $query) use ($auth_tenant_slug) {
                    $query->where('slug', $auth_tenant_slug);
                })
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If cloud provider was not specified, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the cloud provider belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            // Show table in console output
            $this->table(['Provider'], $cloud_providers);

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should the credentials be set for?', Arr::flatten($cloud_providers));

            // Lookup role based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->first();

        }

        // AWS Credentials
        if($cloud_provider->type == 'aws') {

            // AWS Access Key ID
            $aws_access_key_id = $this->option('aws_access_key_id');
            if($aws_access_key_id == null) {
                $aws_access_key_id = $this->ask('What is the access key ID (API username) of the AWS IAM account?');
            }

            // AWS Access Key Secret
            $aws_access_key_secret = $this->option('aws_access_key_secret');
            if($aws_access_key_secret == null) {
                $aws_access_key_secret = $this->ask('What is the access key secret (API password) of the AWS IAM account?');
            }

            // Parent Organization Unit
            $parent_org_unit = $this->option('parent_org_unit');
            if($parent_org_unit == null) {
                if($this->confirm('Do you want to set a parent organization unit that all realms should be created under? If not, all cloud realms will be created in the top-level organization.')) {
                    $parent_org_unit = $this->ask('What is the AWS organizational unit ID (ou-a1b2-c3d4e5f) that should be the parent for all realm organization unit folders.');
                } else {
                    $parent_org_unit = $cloud_provider->parent_org_unit != null ? $cloud_provider->parent_org_unit : null;
                }
            }

            // Future: Implement RegEx checking for access key and secret format
            //
            // Search for access key IDs:
            // (?<![A-Z0-9])[A-Z0-9]{20}(?![A-Z0-9]).
            // In English, this regular expression says: Find me 20-character,
            // uppercase, alphanumeric strings that don’t have any uppercase,
            // alphanumeric characters immediately before or after.
            //
            // Search for secret access keys:
            // (?<![A-Za-z0-9/+=])[A-Za-z0-9/+=]{40}(?![A-Za-z0-9/+=]).
            // In English, this regular expression says: Find me 40-character,
            // base-64 strings that don’t have any base 64 characters
            // immediately before or after.

            $api_credentials = [
                'aws_access_key_id' => $aws_access_key_id,
                'aws_access_key_secret' => $aws_access_key_secret
            ];

            // Uncomment for troubleshooting array values
            // dd(json_decode(decrypt($api_credentials)));

        }

        // GCP Credentials
        elseif($cloud_provider->type == 'gcp') {

            // GCP Credentials File
            $gcp_credentials_file = $this->option('gcp_credentials_file');
            if($gcp_credentials_file == null) {
                $gcp_credentials_file = $this->ask('What is the filename of the JSON file that contains the Google service account credentials in the storage/keys directory?');
            }

            // Parent Organization Unit
            $parent_org_unit = $this->option('parent_org_unit');
            if($parent_org_unit == null) {
                if($cloud_provider->parent_org_unit == null) {
                    if($this->confirm('All cloud realms are currently created at the top-level organization. Do you want to set a parent organization unit that all realms should be created under?')) {
                        $parent_org_unit = $this->ask('What is the GCP folder number (123456789012) that should be the parent for all realm organization unit folders.');
                    } else {
                        $parent_org_unit = null;
                    }
                } elseif($cloud_provider->parent_org_unit != null) {
                    if($this->confirm('Realms are currently created under organization unit '.$cloud_provider->parent_org_unit.'. Do you want to update the parent organization unit that all realms should be created under?')) {
                        $parent_org_unit = $this->ask('What is the GCP folder number (123456789012) that should be the parent for all realm organization unit folders.');
                    } else {
                        $parent_org_unit = $cloud_provider->parent_org_unit;
                    }
                }
            }

            // Validate that file exists and parse JSON
            if(!is_file(storage_path('keys/'.$gcp_credentials_file))) {

                $this->line('<fg=red>No credentials file found in `'.storage_path('keys/'.$gcp_credentials_file).'`</>');
                $this->error('Aborted. No changes have been made.');
                die();

            } else {

                // Get contents of GCP credentials file
                $gcp_credentials_path = storage_path('keys/'.$gcp_credentials_file);
                $gcp_credentials_file_contents = file_get_contents($gcp_credentials_path);

                // Uncomment for troubleshooting array values
                // dd(json_decode($gcp_credentials_file_contents));

                // Decode JSON file
                $api_credentials = json_decode($gcp_credentials_file_contents);

                // Check that JSON file has the proper format by checking for the `type` key
                if(!isset($api_credentials->type)) {
                    $this->line('<fg=red>The credentials file found in `'.$gcp_credentials_path.'` is not able to be parsed.</>');
                    $this->error('Aborted. No changes have been made.');
                    $this->line('');
                    die();
                }

                // Check that array type key is service_account
                if($api_credentials->type != 'service_account') {
                    $this->line('<fg=red>The credentials file found in `'.$gcp_credentials_path.'` is not for a service account.</>');
                    $this->error('Aborted. No changes have been made.');
                    $this->line('');
                    die();
                }

                // Check if key folder exists
                $keys_dir = storage_path('keys');
                if(!is_dir($keys_dir)){
                  mkdir($keys_dir, 0770, true);
                  chown(storage_path($keys_dir), config('hackystack.storage.chown_user'));
                  chgrp(storage_path($keys_dir), config('hackystack.storage.chown_group'));
                }

                // Check if cloud provider key folder exists
                $cloud_providers_dir = storage_path('keys/cloud_providers');
                if(!is_dir($cloud_providers_dir)){
                  mkdir($cloud_providers_dir, 0770, true);
                  chown(storage_path($cloud_providers_dir), config('hackystack.storage.chown_user'));
                  chgrp(storage_path($cloud_providers_dir), config('hackystack.storage.chown_group'));
                }

                // Create new file for GCP credentials
                $cloud_provider_credentials_file_path = storage_path('keys/cloud_providers/'.$cloud_provider->id.'.json');
                rename($gcp_credentials_path, $cloud_provider_credentials_file_path);
                chmod($cloud_provider_credentials_file_path, 0640);
                chown($cloud_provider_credentials_file_path, config('hackystack.storage.chown_user'));
                chgrp($cloud_provider_credentials_file_path, config('hackystack.storage.chown_group'));

            }

        }

        // Use service method to update cloud provider
        $cloudProviderService = new Services\V1\Cloud\CloudProviderService();
        $cloudProviderService->update($cloud_provider->id, [
            'api_credentials' => $api_credentials,
            'parent_org_unit' => $parent_org_unit
        ]);

        $this->info('API credentials updated successfully.');

    }
}
