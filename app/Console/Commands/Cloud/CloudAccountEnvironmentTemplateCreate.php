<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudAccountEnvironmentTemplateCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment-template:create
                            {--T|auth_tenant_slug= : The slug of the tenant this environment template belongs to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider this environment template belongs to}
                            {--I|git_project_id= : The ID (integer) of the Git project in the Git instance}
                            {--N|name= : The display name of this environment template. If blank, the Git project name from the API will be shown.}
                            {--D|description= : The display description of this environment template. If blank, the Git project description from the API will be shown.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Cloud Account Environment Template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Account Environment Template - Create Record');

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this cloud account belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Provider
        // --------------------------------------------------------------------
        //

        // Get provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->line('');
            $this->line('Available cloud providers: '.implode(',', Arr::flatten($cloud_providers)));

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should this cloud account belong to?', Arr::flatten($cloud_providers));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_provider) {
            $this->error('Error: No cloud provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Git Project ID
        $git_project_id = $this->option('git_project_id');
        if($git_project_id == null) {
            $git_project_id = $this->ask('What is the ID (integer) of the Git project in the Git instance?');
        }

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this environment template? If blank, the Git project name from the API will be shown.');
        }

        // Description
        $description = $this->option('description');
        if($description == null) {
            $description = $this->ask('What is the display description of this environment template? If blank, the Git project description from the API will be shown.');
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant_id',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'cloud_provider_id',
                        '['.$cloud_provider->short_id.'] '.$cloud_provider->slug
                    ],
                    [
                        'git_project_id',
                        $git_project_id
                    ],
                    [
                        'name',
                        $name
                    ],
                    [
                        'description',
                        $description
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $cloudAccountEnvironmentTemplateService = new Services\V1\Cloud\CloudAccountEnvironmentTemplateService();

        // Use service to create record
        $record = $cloudAccountEnvironmentTemplateService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_provider_id' => $cloud_provider->id,
            'git_project_id' => $git_project_id,
            'name' => $name,
            'description' => $description,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-account-environment-template:get', [
            'short_id' => $record->short_id
        ]);

    }

}
