<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class CloudAccountUserRoleCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-user-role:create
                            {--T|auth_tenant_slug= : The slug of the tenant that the group exists in}
                            {--C|cloud_account_short_id= : The short ID of the Cloud Account}
                            {--U|cloud_account_user_short_id= : The short ID of the Cloud Account User}
                            {--R|cloud_account_role_short_id= : The short ID of the Cloud Account Role}
                            {--E|expires_at= : The date (YYYY-MM-DD) that this role expires}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach a Role to an Cloud Account User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Account User Role - Create Record');

        //
        // Authentication Tenant
        // --------------------------------------------------------------------
        // Lookup the authentication tenant or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the user exist in?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Account
        // --------------------------------------------------------------------
        // Lookup the cloud account or provide list of autocomplete options for
        // the user to choose from.
        //

        // Get cloud account from console option
        $cloud_account_short_id = $this->option('cloud_account_short_id');

        // If cloud account ID option was specified, lookup by ID
        if($cloud_account_short_id) {
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('short_id', $cloud_account_short_id)
                ->first();
        }

        // If cloud account ID was not provided, prompt for input
        else {

            // Get list of groups to show in console
            $cloud_accounts = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->table(
                ['Cloud Accounts'],
                $cloud_accounts
            );

            $cloud_account_prompt = $this->anticipate('Which cloud account should the user be attached to?', Arr::flatten($cloud_accounts));

            // Lookup group based on slug provided in prompt
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_account_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$cloud_account) {
            $this->error('Error: No cloud account was found.');
            $this->error('');
            die();
        }

        //
        // Cloud Account User
        // --------------------------------------------------------------------
        // Lookup the cloud account user or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get cloud account user from console option
        $cloud_account_user_short_id = $this->option('cloud_account_user_short_id');

        // If option was specified, lookup by ID
        if($cloud_account_user_short_id) {
            $cloud_account_user = Models\Cloud\CloudAccountUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_account_id', $cloud_account->id)
                ->where('short_id', $cloud_account_user_short_id)
                ->first();
        }

        // If option was not provided, prompt for input
        else {

            // Get list of records to show in console
            $cloud_accounts_users = Models\Cloud\CloudAccountUser::query()
                ->where('cloud_account_id', $cloud_account->id)
                ->get(['username'])
                ->toArray();

            $this->table(
                ['Username'],
                $cloud_accounts_users
            );

            $cloud_account_user_prompt = $this->anticipate('Which cloud account user should the role be attached to?', Arr::flatten($cloud_accounts_users));

            // Lookup group based on slug provided in prompt
            $cloud_account_user = Models\Cloud\CloudAccountUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_account_id', $cloud_account->id)
                ->where('username', $cloud_account_user_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$cloud_account_user) {
            $this->error('Error: No cloud account user was found.');
            $this->error('');
            die();
        } else {
            $cloud_account_user_id = $cloud_account_user->id;
        }

        //
        // Cloud Account Role
        // --------------------------------------------------------------------
        // Lookup the cloud account roles or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get cloud account role from console option
        $cloud_account_role_short_id = $this->option('cloud_account_role_short_id');

        // If option was specified, lookup by ID
        if($cloud_account_role_short_id) {
            $cloud_account_role = Models\Cloud\CloudAccountRole::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_account_id', $cloud_account->id)
                ->where('short_id', $cloud_account_role_short_id)
                ->first();
        }

        // If option was not provided, prompt for input
        else {

            // Get list of records to show in console
            $cloud_account_roles = Models\Cloud\CloudAccountRole::query()
                ->where('cloud_account_id', $cloud_account->id)
                ->whereDoesntHave('cloudAccountUserRoles', function (Builder $query) use ($cloud_account_user_id) {
                    $query->where('cloud_account_user_id', $cloud_account_user_id);
                })
                ->get(['api_name'])
                ->toArray();

            $this->table(
                ['Role Name'],
                $cloud_account_roles
            );

            $cloud_account_role_prompt = $this->anticipate('Which cloud account role should the user be attached to?', Arr::flatten($cloud_account_roles));

            // Lookup group based on slug provided in prompt
            $cloud_account_role = Models\Cloud\CloudAccountRole::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_account_id', $cloud_account->id)
                ->where('api_name', $cloud_account_role_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$cloud_account_role) {
            $this->error('Error: No cloud account role was found.');
            $this->error('');
            die();
        }

        //
        // Additional String Parameters and Options
        // --------------------------------------------------------------------
        //

        // Expires at (not prompted, only as CLI option)
        $expires_at = $this->option('expires_at');
        if($expires_at != null) {
            $expires_at = \Carbon\Carbon::parse($expires_at)->format('Y-m-d');
        } else {
            $expires_at = null;
        }

        //
        // Verify inputs table
        // --------------------------------------------------------------------
        // Show the user the database values that will be added before saving
        // changes.
        //

        $this->table(
            ['Column', 'Value'],
            [
                [
                    'auth_tenant_id',
                    '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                ],
                [
                    'cloud_account_id',
                    '['.$cloud_account->short_id.'] '.$cloud_account->slug
                ],
                [
                    'cloud_account_user_id',
                    '['.$cloud_account_user->short_id.'] '.$cloud_account_user->username
                ],
                [
                    'cloud_account_role_id',
                    '['.$cloud_account_role->short_id.'] '.$cloud_account_role->api_name
                ],
                [
                    'expires_at',
                    $expires_at
                ],
            ]
        );

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the creation of the record?')) {
            $this->error('Error: You aborted. No record was created.');
            die();
        }

        // Initialize service
        $cloudAccountUserRoleService = new Services\V1\Cloud\CloudAccountUserRoleService();

        // Use service to create record
        $record = $cloudAccountUserRoleService->store([
            'cloud_account_role_id' => $cloud_account_role->id,
            'cloud_account_user_id' => $cloud_account_user->id,
            'expires_at' => $expires_at,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

    }

}
