<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class CloudAccountUserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-user:create
                            {--T|auth_tenant_slug= : The slug of the tenant that the group exists in}
                            {--U|auth_user_short_id= : The short ID of the Authentication User}
                            {--C|cloud_account_short_id= : The short ID of the Cloud Account}
                            {--E|expires_at= : The date (YYYY-MM-DD) that this user expires}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach a User to an Cloud Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Account Users - Create Record');

        //
        // Authentication Tenant
        // --------------------------------------------------------------------
        // Lookup the authentication tenant or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the user exist in?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Account
        // --------------------------------------------------------------------
        // Lookup the cloud account or provide list of autocomplete options for
        // the user to choose from.
        //

        // Get cloud account from console option
        $cloud_account_short_id = $this->option('cloud_account_short_id');

        // If cloud account ID option was specified, lookup by ID
        if($cloud_account_short_id) {
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('short_id', $cloud_account_short_id)
                ->first();
        }

        // If cloud account ID was not provided, prompt for input
        else {

            // Get list of groups to show in console
            $cloud_accounts = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->table(
                ['Cloud Accounts'],
                $cloud_accounts
            );

            $cloud_account_prompt = $this->anticipate('Which cloud account should the user be attached to?', Arr::flatten($cloud_accounts));

            // Lookup group based on slug provided in prompt
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_account_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$cloud_account) {
            $this->error('Error: No cloud account was found.');
            $this->error('');
            die();
        }

        //
        // Authentication User
        // --------------------------------------------------------------------
        // Lookup the authentication users or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth user from console option
        $auth_user_short_id = $this->option('auth_user_short_id');

        // If auth user short ID option was specified, lookup by ID
        if($auth_user_short_id) {
            $auth_user = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('short_id', $auth_user_short_id)
                ->first();
        }

        // If auth user short ID was not provided, prompt for input
        else {

            // Get list of users to show in console
            $cloud_account_id = $cloud_account->id;
            $auth_users = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->whereDoesntHave('cloudAccountUsers', function (Builder $query) use ($cloud_account_id) {
                    $query->where('cloud_account_id', $cloud_account_id);
                })->get(['email'])
                ->toArray();

            $this->line('You can view the database table or use this command in a separate terminal window to get a list of users:');
            $this->line('auth-user:list -T '.$auth_tenant->slug);
            $this->line('');

            $auth_user_prompt = $this->anticipate('What is the email address of the user that you want to attach to the cloud account?', Arr::flatten($auth_users));

            // Lookup group based on slug provided in prompt
            $auth_user = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('email', $auth_user_prompt)
                ->first();

            // Verify that user does not belong to Cloud Account
            $cloud_account_id = $cloud_account->id;
            $auth_user_existing_cloud_account = Models\Auth\AuthUser::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('email', $auth_user_prompt)
                ->whereHas('cloudAccountUsers', function (Builder $query) use ($cloud_account_id) {
                    $query->where('cloud_account_id', $cloud_account_id);
                })->first();

            // Validate that users doesn't already have an existing relationship
            // with this group, or return error message
            if($auth_user_existing_cloud_account) {
                $this->error('Error: This user is already associated with this cloud account.');
                $this->error('');
                die();
            }

        }

        // Validate that users exists or return error message
        if(!$auth_user) {
            $this->error('Error: No user was found with that short ID.');
            $this->error('');
            die();
        }

        //
        // Additional String Parameters and Options
        // --------------------------------------------------------------------
        //

        // Expires at (not prompted, only as CLI option)
        $expires_at = $this->option('expires_at');
        if($expires_at != null) {
            $expires_at = \Carbon\Carbon::parse($expires_at)->format('Y-m-d');
        } else {
            $expires_at = null;
        }

        //
        // Verify inputs table
        // --------------------------------------------------------------------
        // Show the user the database values that will be added before saving
        // changes.
        //

        $this->table(
            ['Column', 'Value'],
            [
                [
                    'auth_tenant_id',
                    '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                ],
                [
                    '[relationship] cloudAccount',
                    '['.$cloud_account->short_id.'] '.$cloud_account->slug
                ],
                [
                    '[relationship] authUser',
                    '['.$auth_user->short_id.'] '.$auth_user->email
                ],
                [
                    'expires_at',
                    $expires_at ? $expires_at : 'never'
                ],
            ]
        );

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the creation of the record?')) {
            $this->error('Error: You aborted. No record was created.');
            die();
        }

        // Initialize service
        $cloudAccountUserService = new Services\V1\Cloud\CloudAccountUserService();

        // Use service to create record
        $record = $cloudAccountUserService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_account_id' => $cloud_account->id,
            'auth_user_id' => $auth_user->id,
            'expires_at' => $expires_at,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-account-user:get', [
            'short_id' => $record->short_id,
            '--with-password' => true
        ]);

        $this->comment('This Cloud Account User does not have any permissions.');
        $this->error('Use `cloud-account-user-role:create` to grant permissions.');

    }

}
