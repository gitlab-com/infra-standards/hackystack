<?php

namespace App\Actions\Connections\Google;

use App\Actions\Connections\Google\Exceptions\AuthenticationException;
use App\Actions\Connections\Google\Exceptions\ConfigurationException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Google API Authentication Token Generator
 *
 * @author Dillon Wheeler
 * @author Jeff Martin
 *
 * This is used to authenticate with the Google OAuth2 Sever utilizing a Google Service Account JSON API key.
 *
 * The OAUTH service will return a short-lived API token that can be used with the Laravel HTTP Client to perform
 * GET, POST, PATCH, DELETE, etc. API requests that can be found in the Google API Explorer documentation.
 *
 * You can configure your connections (ex. `workspace`, `cloud`, etc.) in the `config/connections.php` and `.env` file.
 *
 * To provide a streamlined developer experience, this uses either the json_key_file_path parameter that points
 * to your JSON API key's storage path, or you can provide the JSON API key as a string in the json_key parameter.
 *
 * This is called from the inside ApiClient class and does not need to be instantiated separately in your code.
 */
class ApiToken
{
    use AsAction;

    // Standard parameters for building JWT request with Google OAuth Server.
    // They are put here for easy changing if necessary
    public const AUTH_BASE_URL = 'https://oauth2.googleapis.com/token';
    public const AUTH_ALGORITHM = 'RS256';
    public const AUTH_TYPE = 'JWT';
    public const AUTH_GRANT_TYPE = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
    public const ENCRYPT_METHOD = 'sha256';

    /**
     * Standard initialization construct method.
     *
     * @param ?string $connection_key
     *      The connection key in config/connections.php. Set to null to use default connection key.
     *
     * @throws AuthenticationException
     */
    public function handle(?string $connection_key = null)
    {
        $connection_key = ($connection_key ?? config('connections.default.google'));

        // Define connection configuration
        self::validateConnectionKey($connection_key);
        self::validateConnectionConfigArray($connection_key);

        $connection_config = config('connections.google.' . $connection_key);

        // Parse the JSON key and return an object
        $json_key_contents = self::getJsonKeyContents($connection_key);

        // Convert API scopes to space separated list
        $api_scopes = collect($connection_config['api_scopes'])->implode(' ');

        // If subject email is not supplied set the subject_email to the client_email
        $subject_email = ($connection_config['subject_email'] ?? $json_key_contents->client_email);
        $client_email = ($json_key_contents->client_email ?? null);

        // Create the encrypted JWT Headers
        $jwt_headers = self::createJwtHeader();

        // Create the encrypted JWT Claim
        $jwt_claim = self::createJwtClaim($client_email, $api_scopes, $subject_email);

        // Create the signature to append to the JWT
        $signature = self::createSignature(
            $jwt_headers,
            $jwt_claim,
            $json_key_contents->private_key
        );

        // Set the class jwt variable to the Google OAuth2 required string
        $jwt = $jwt_headers . '.' . $jwt_claim . '.' . $signature;

        // Send the authentication request with the `jwt` and return the access_token from the response
        $response = self::sendAuthRequest($connection_key, $jwt);

        if (!property_exists($response->object(), 'access_token')) {
            throw new AuthenticationException('Google API authentication failed for `' . $connection_key . '`');
        } else {
            return $response->object()->access_token;
        }
    }

    /**
     * Set the connection_key class property variable
     *
     * @param string $connection_key
     *      The connection key in config/connections.php
     *
     * @throws ConfigurationException
     */
    private static function validateConnectionKey(string $connection_key): void
    {
        if (!array_key_exists($connection_key, config('connections.google'))) {
            throw new ConfigurationException(
                'Google API configuration missing for `' . $connection_key . '`. ' .
                    'The key is not defined in the `config/connections.php` google array.'
            );
        }
    }

    /**
     * Validate that array keys in `REQUIRED_CONFIG_PARAMETERS` exists in the `connection_config`
     *
     * Loop through each of the required parameters in `REQUIRED_CONFIG_PARAMETERS` and verify that each of them are
     * contained in the array for the connection key. If there is a key missing an error will be logged.
     *
     * @param string $connection_key
     *      The connection key in config/connections.php
     *
     * @throws ConfigurationException
     */
    private static function validateConnectionConfigArray($connection_key): void
    {
        $connection_config = config('connections.google.' . $connection_key);

        $validator = Validator::make(
            $connection_config,
            [
                'api_scopes' => 'required|array',
                'customer_id' => 'nullable|string',
                'domain' => 'nullable|string',
                'subject_email' => 'nullable|string',
                'json_key_file_path' => Rule::requiredIf(!array_key_exists('json_key', $connection_config)) . '|string|nullable',
                'json_key' => Rule::requiredIf(!array_key_exists('json_key_file_path', $connection_config)) . '|string|nullable',
                'log_channels' => 'nullable|array'
            ],
            [
                'json_key_file_path.required' => 'Either the json_key_file_path or json_key parameters are required',
                'json_key.required' => 'Either the json_key_file_path or json_key parameters are required'
            ]
        );

        if ($validator->fails()) {
            throw new ConfigurationException(
                'Google API configuration validation error for `' . $connection_key . '`. ' .
                    $validator->messages()->first()
            );
        } else {
            Log::debug('Google API configuration validation successful for `' . $connection_key . '`');
        }
    }

    /**
     * Get Google API JSON key contents from `json_key` or `json_key_file_path` connection configuration.
     *
     * @param string $connection_key
     *      The connection key in config/connections.php
     *
     * @throws ConfigurationException
     */
    private static function getJsonKeyContents($connection_key): object
    {
        $connection_config = config('connections.google.' . $connection_key);

        if (isset($connection_config['json_key'])) {
            $json_key = json_decode($connection_config['json_key']);
        } elseif (isset($connection_config['json_key_file_path'])) {
            $json_key = json_decode((string) file_get_contents($connection_config['json_key_file_path']));
        }

        if (empty($json_key)) {
            throw new ConfigurationException(
                'Google API validation error. The `' . $connection_key . '` JSON key contents are empty.'
            );
        } else {
            return $json_key;
        }
    }

    /**
     * Create and encode the required JWT Headers for Google OAuth2 authentication
     *
     * @see https://developers.google.com/identity/protocols/oauth2/service-account#:~:text=Forming%20the%20JWT%20header
     */
    private static function createJwtHeader(): string
    {
        return self::base64UrlEncode((string) json_encode([
            'alg' => self::AUTH_ALGORITHM,
            'typ' => self::AUTH_TYPE,
        ]));
    }

    /**
     * Encoding schema utilized by Google OAuth2 Servers
     *
     * @see https://stackoverflow.com/a/65893524
     *
     * @param string $input
     *      The input string to encode
     */
    private static function base64UrlEncode(string $input): string
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    /**
     * Create and encode the required JWT Claims for Google OAuth2 authentication
     *
     * @see https://developers.google.com/identity/protocols/oauth2/service-account#:~:text=Forming%20the%20JWT%20claim%20set
     *
     * @param string $client_email
     *      The `client_email` from the Google JSON key
     *
     * @param string $api_scopes
     *      The `api_scopes` from the handle method
     *
     * @param string $subject_email
     *      The `subject_email` to use for authentication
     */
    private static function createJwtClaim(string $client_email, string $api_scopes, string $subject_email): string
    {
        return self::base64UrlEncode((string)json_encode([
            'iss' => $client_email,
            'scope' => $api_scopes,
            'aud' => self::AUTH_BASE_URL,
            'exp' => time() + 3600,
            'iat' => time(),
            'sub' => $subject_email
        ]));
    }

    /**
     * Create a OpenSSL signature using JWT Header and Claim and the private_key from the Google JSON key
     *
     * @see https://developers.google.com/identity/protocols/oauth2/service-account#:~:text=Computing%20the-,signature,-JSON%20Web%20Signature
     *
     * @see https://datatracker.ietf.org/doc/html/rfc7515
     *
     * @see https://www.php.net/manual/en/function.openssl-pkey-get-private.php
     *
     * @param string $jwt_header
     *      The JWT Header string required for Google OAuth2 authentication
     *
     * @param string $jwt_claim
     *      The JWT Claim string required for Google OAuth2 authentication
     *
     * @param string $private_key
     *      The Google JSON key `private_key` value
     */
    private static function createSignature(string $jwt_header, string $jwt_claim, string $private_key): string
    {
        $key_id = openssl_pkey_get_private($private_key);

        openssl_sign(
            $jwt_header . '.' . $jwt_claim,
            $private_key,
            $key_id,
            self::ENCRYPT_METHOD
        );

        return self::base64UrlEncode($private_key);
    }

    /**
     * Create and send the Google Authentication POST request
     *
     * @see https://developers.google.com/identity/protocols/oauth2/service-account#:~:text=Making%20the%20access%20token%20request
     *
     * @param string $connection_key
     *      The connection key in config/connections.php
     *
     * @param string $jwt
     *      The JWT to use for authentication
     */
    private static function sendAuthRequest(string $connection_key, string $jwt): object
    {
        $response = Http::asForm()->post(
            self::AUTH_BASE_URL,
            [
                'grant_type' => self::AUTH_GRANT_TYPE,
                'assertion' => $jwt
            ]
        );

        if (!$response->successful()) {
            if (property_exists($response->object(), 'error')) {
                throw new AuthenticationException(
                    'Google API authentication error for `' . $connection_key . '` connection. ' . $response->object()->error_description
                );
            } else {
                throw new AuthenticationException(
                    'Google API authentication error for `' . $connection_key . '` connection. Failed due to error in the sendAuthRequest method.'
                );
            }
        }

        Log::debug('Google API authentication successful for `' . $connection_key . '` connection.');

        return $response;
    }
}
