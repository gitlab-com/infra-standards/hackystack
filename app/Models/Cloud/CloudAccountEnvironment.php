<?php

namespace App\Models\Cloud;

use App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CloudAccountEnvironment extends Models\BaseModel
{
    use SoftDeletes;

    public $table = 'cloud_accounts_environments';

    // Hide these fields from arrays
    protected $hidden = [
        //
    ];

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    protected $fillable = [
        'auth_tenant_id',
        'cloud_account_id',
        'cloud_account_environment_template_id',
        'cloud_provider_id',
        'name',
        'description',
        'provisioned_at',
        'expires_at',
        'state'
    ];

    //
    // Form Validation Rules
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/validation#available-validation-rules
    // See additional documentation in App\Models\BaseModel about how we use
    // form validation rules and implementation examples.
    //

    public $storeRules = [
        'auth_tenant_id' => 'required|uuid|exists:auth_tenants,id',
        'cloud_account_id' => 'required|uuid|exists:cloud_accounts,id',
        'cloud_account_environment_template_id' => 'required|uuid|exists:cloud_accounts_environments_templates,id',
        'cloud_provider_id' => 'required|uuid|exists:cloud_providers,id',
        'git_meta_data' => 'nullable|array',
        'name' => 'required|string',
        'description' => 'nullable|string',
        'provisioned_at' => 'nullable|datetime',
        'expires_at' => 'nullable|datetime',
    ];

    public $updateRules = [
        'git_meta_data' => 'nullable|array',
        'name' => 'nullable|string',
        'description' => 'nullable|string',
        'expires_at' => 'nullable|datetime',
        'state' => 'nullable|string|in:active,inactive,archived'
    ];

    //
    // If the default error messages are not friendly enough for users, you
    // can set a custom error message for each field and condition not met.
    // https://laravel.com/docs/6.x/validation#custom-error-messages
    //

    public $validationMessages = [
        // 'field.condition' => 'This is a custom error message.'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response. This is not
    // required since the database column type is used by default, however we
    // want to be explicit for engineering clarity.
    //

    protected $casts = [
        // Common fields
        'id' => 'string',
        'short_id' => 'string',
        'created_at' => 'datetime',
        'created_by' => 'string',
        'updated_at' => 'datetime',
        'updated_by' => 'string',
        'deleted_at' => 'datetime',
        'deleted_by' => 'string',
        // Model specific fields
        'auth_tenant_id' => 'string',
        'cloud_account_id' => 'string',
        'cloud_account_environment_template_id' => 'string',
        'cloud_provider_id' => 'string',
        'git_meta_data' => 'array',
        'git_ci_variables' => 'array',
        'git_project_id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'provisioned_at' => 'datetime',
        'expires_at' => 'datetime',
        'state' => 'string',
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'provisioned_at',
        'expires_at',
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`packageWidget`). For a child
    // or many-to-many relationship, use a plural name (`packageWidgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function authTenant() {
        return $this->belongsTo(Models\Auth\AuthTenant::class, 'auth_tenant_id');
    }

    public function cloudAccount() {
        return $this->belongsTo(CloudAccount::class, 'cloud_account_id');
    }

    public function cloudAccountEnvironmentTemplate() {
        return $this->belongsTo(CloudAccountEnvironmentTemplate::class, 'cloud_account_environment_template_id');
    }

    public function cloudOrganizationUnit() {
        return $this->belongsTo(CloudOrganizationUnit::class, 'cloud_organization_unit_id');
    }

    public function cloudProvider() {
        return $this->belongsTo(CloudProvider::class, 'cloud_provider_id');
    }

    public function cloudRealm() {
        return $this->belongsTo(CloudRealm::class, 'cloud_realm_id');
    }

    public function createdBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'created_by');
    }

    public function updatedBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'updated_by');
    }

    public function deletedBy() {
        return $this->belongsTo(Models\Auth\AuthUser::class, 'deleted_by');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //     DistantModel::class,
    //     IntermediateModel::class,
    //     'intermediate_key_id',
    //     'distant_key_id',
    //     'id'
    // );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(
    //     ForeignModel::class,
    //     'package_this_foreign_table',
    //     'this_table_id',
    //     'foreign_table_id'
    // )->withPivot([
    //     'id',
    //     'field_name',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at'
    // ])->as('pivotModelName');
    //

    //
    // Custom Attribute Getters
    // ------------------------------------------------------------------------
    // When getting attributes in the database, these methods add additional
    // business logic to be invoked when a field is fetched.
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

    // TODO Scope for state of environment.

}
