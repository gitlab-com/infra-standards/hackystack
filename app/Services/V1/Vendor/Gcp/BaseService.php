<?php

namespace App\Services\V1\Vendor\Gcp;

use App\Models\Cloud\CloudProvider;
use Exception;
use Glamstack\GoogleCloud\ApiClient;
use Google_Client;

trait BaseService
{
    private $cloud_provider;
    private Google_Client $google_api_client;
    private string $json_key_file_path;
    private string $api_token;
    private string $project_id;

    /**
     * Get the GCP Project ID class variable value
     *
     * @see https://cloud.google.com/resource-manager/docs/creating-managing-projects
     *
     * @return string
     *      ```
     *      (example) 123456789012
     *      (example) my-project-a1b2c3
     *      ```
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Set the GCP Project ID class variable
     *
     * @see https://cloud.google.com/resource-manager/docs/creating-managing-projects
     *
     * @param string $project_id
     *      ```
     *      (example) 123456789012
     *      (example) my-project-a1b2c3
     *      ```
     *
     * @return void
     */
    public function setProjectId(string $project_id): void
    {
        $this->project_id = $project_id;
    }

    /**
     * Get the cloud provider object
     *
     * @return mixed
     */
    public function getCloudProvider(): mixed
    {
        return $this->cloud_provider;
    }

    /**
     * Set the Cloud Provider model as a class variable
     *
     * @param string $cloud_provider_id
     *      The UUID of the Cloud Provider with GCP type
     *
     * @return void
     */
    public function setCloudProvider($cloud_provider_id): void
    {
        // Get cloud provider from database
        $this->cloud_provider = CloudProvider::query()
            ->where('id', $cloud_provider_id)
            ->firstOrFail();

        // Check if cloud provider type is not correct
        if ($this->cloud_provider->type != 'gcp') {
            abort(500, 'The GCP service is trying to use a cloud provider that is not the correct type.', [
                'cloud_provider_id' => $this->cloud_provider->id,
                'cloud_provider_type' => $this->cloud_provider->type,
            ]);
        }
    }

    /**
     * Get the value of `json_key_file_path` parameter
     *
     * @return string
     */
    public function getJsonKeyFilePath(): string
    {
        return $this->json_key_file_path;
    }

    /**
     * Set the `json_key_file_path` parameter
     *
     * @return void
     */
    protected function setJsonKeyFilePath(): void
    {
        $this->json_key_file_path = storage_path('keys/cloud_providers/' . $this->cloud_provider->id . '.json');
    }

    /**
     * Use Google Vendor SDK to create an API Connection
     *
     * Deprecation Notice: This method has been deprecated and is only used by
     * methods that have not been updated to use the Glamstack SDK. This will be
     * removed in a future release.
     *
     * @return void
     *
     * @throws \Google\Exception
     */
    public function setGoogleApiClient(): void
    {
        // Connect to Google API using Cloud Provider credentials
        // https://github.com/googleapis/google-api-php-client#authentication-with-service-accounts
        // https://developers.google.com/my-business/samples#detailed_error_responses
        $this->google_api_client = new \Google_Client();
        $this->google_api_client->setAuthConfig(storage_path('keys/cloud_providers/' . $this->cloud_provider->id . '.json'));
        $this->google_api_client->setApiFormatV2(true);
    }

    /**
     * Initialize `glamstack/google-cloud-sdk`
     *
     * @see https://gitlab.com/gitlab-com/business-technology/engineering/access-manager/packages/composer/google-cloud-sdk/-/blob/main/README.md
     *
     * @param array $api_scopes
     *      The api_scopes for the API calls
     *
     * @param string $json_key_file_path
     *      The file path of the Google API JSON key file
     *
     * @param string $project_id
     *      The GCP Project ID to initialize the SDK with
     *
     * @return ApiClient
     */
    public function setGlamstackGoogleCloudSdk(array $api_scopes, string $json_key_file_path, string $project_id): ApiClient
    {
        return new ApiClient(null, [
            'api_scopes' => $api_scopes,
            'json_key_file_path' => $json_key_file_path,
            'project_id' => $project_id,
        ]);
    }

    /**
     * Handles exception messages
     *
     * @param Exception $e
     *      The exception to handle
     *
     * @return void
     */
    public function handleException(Exception $e): void
    {
        $error_message = json_decode($e->getMessage());
        dd($error_message->error->code . ' ' . $error_message->error->message);
    }
}
