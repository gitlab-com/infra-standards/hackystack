<?php

namespace App\Services\V1\Vendor\Gcp;

use App\Models\Cloud\CloudProvider;
use Glamstack\GoogleCloud\ApiClient;
use Exception;

class CloudDnsRecordSetService
{
    use BaseService;

    const API_SCOPES = [
        'https://www.googleapis.com/auth/ndev.clouddns.readwrite'
    ];

    private ApiClient $api_client;

    public function __construct($cloud_provider, string $project_id, string $gcp_suffix = '.gcp.gitlabsandbox.net.')
    {
        // Set the cloud_provider_id
        $this->setCloudProvider($cloud_provider);

        $this->setJsonKeyFilePath();

        $this->setProjectId($project_id);

        $this->api_client = $this->setGlamstackGoogleCloudSdk(
            self::API_SCOPES,
            $this->json_key_file_path,
            $this->project_id
        );
    }

    public function createDnsRecordSet(array $request_data): object
    {
        $request_data['project_id'] = $request_data['project_id'] ?? $this->project_id;

        // Check if record exists
        $record_exists = $this->checkForDnsRecordSet([
            'managed_zone' => $request_data['managed_zone'],
            'name' => $request_data['name'],
            'type' => $request_data['type'],
            'project_id' => $request_data['project_id']
        ]);

        if ($record_exists->status->code == 404) {
            return $this->createGcpDnsRecordSet($request_data);
        } else {
            if (property_exists($record_exists->object, 'error')) {
                throw new Exception($record_exists->object->error->message);
            } else {
                throw new Exception($record_exists->json);
            }
        }
    }

    public function deleteDnsRecordSet($request_data)
    {
        $request_data['project_id'] = $request_data['project_id'] ?? $this->project_id;

        // Check if record exists
        $record_exists = $this->checkForDnsRecordSet([
            'managed_zone' => $request_data['managed_zone'],
            'name' => $request_data['name'],
            'type' => $request_data['type'],
            'project_id' => $request_data['project_id']
        ]);

        if ($record_exists->status->code == 404) {
            if (property_exists($record_exists->object, 'error')) {
                throw new Exception($record_exists->object->error->message);
            } else {
                throw new Exception($record_exists->json);
            }
        } else {
            return $this->deleteGcpDnsRecordSet($request_data);
        }
    }

    /**
     * @throws \Exception
     */
    protected function checkForDnsRecordSet(array $request_data = []): object|string
    {
        $request_data['project_id'] = $request_data['project_id'] ?? $this->project_id;

        return $this->api_client->dns()->recordSet()->get(
            $request_data
        );
    }

    protected function createGcpDnsRecordSet($request_data)
    {
        return $this->api_client->dns()->recordSet()->create($request_data);
    }

    protected function deleteGcpDnsRecordSet($request_data)
    {
        return $this->api_client->dns()->recordSet()->delete($request_data);
    }
}
