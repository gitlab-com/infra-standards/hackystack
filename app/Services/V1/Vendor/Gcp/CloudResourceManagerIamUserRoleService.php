<?php

namespace App\Services\V1\Vendor\Gcp;

use Glamstack\GoogleCloud\ApiClient;
use Illuminate\Support\Str;

class CloudResourceManagerIamUserRoleService
{
    use BaseService;

    public const API_SCOPES = [
        'https://www.googleapis.com/auth/cloud-platform',
        'https://www.googleapis.com/auth/compute'
    ];

    /* GCP Project ID */
    private string $project_id;

    /* GCP JSON API Key File Path */
    private string $json_key_file_path;

    /* glamstack/google-cloud-sdk */
    private ApiClient $api_client;

    /**
     * Initialize the IAM User Role Service
     *
     * @param string $cloud_provider_id
     *      The UUID of the CloudProvider record
     *
     * @param string $project_id
     *      The Google Project ID of the IAM permissions to update
     *
     * @note Once we have the IAM endpoints setup in the SDK we can remove the
     * project_id input variable
     */
    public function __construct($cloud_provider_id, string $project_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);
        $this->setProjectId($project_id);
        $this->setJsonKeyFilePath();
        $this->api_client = $this->setGlamstackGoogleCloudSdk(self::API_SCOPES, $this->json_key_file_path, $this->project_id);
    }

    /**
     * Set the `json_key_file_path` parameter
     *
     * @return void
     */
    protected function setJsonKeyFilePath(): void
    {
        $this->json_key_file_path = storage_path('keys/cloud_providers/' . $this->cloud_provider->id . '.json');
    }

    /**
     * Get the value of `json_key_file_path` parameter
     *
     * @return string
     */
    public function getJsonKeyFilePath(): string
    {
        return $this->json_key_file_path;
    }
    /**
     * Set the GCP Project ID class variable
     *
     * @see https://cloud.google.com/resource-manager/docs/creating-managing-projects
     *
     * @param string $project_id
     *      ```
     *      (example) 123456789012
     *      (example) my-project-a1b2c3
     *      ```
     *
     * @return void
     */
    public function setProjectId(string $project_id): void
    {
        // Check if project has prefix of `projects/` and remove if exists
        if (Str::startsWith($project_id, 'projects/')) {
            $this->project_id = str_replace('projects/', '', $project_id);
        } else {
            $this->project_id = $project_id;
        }
    }

    /**
     * Get the GCP Project ID class variable value
     *
     * @see https://cloud.google.com/resource-manager/docs/creating-managing-projects
     *
     * @return string
     *      ```
     *      (example) 123456789012
     *      (example) my-project-a1b2c3
     *      ```
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Use Google API to get existing IAM policy and bindings for a project
     *
     * @return object
     *      ```json
     *      {
     *          "version": 1,
     *          "etag": "A1b2+C3D4e5=",
     *          "bindings": [
     *              {
     *                  "role": "roles/compute.admin",
     *                  "members": [
     *                      "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
     *                  ],
     *              },
     *              {
     *                  "role": "roles/owner",
     *                  "members": [
     *                      "user:dmurphy@example.com",
     *                      "user:klibby@example.com"
     *                  ],
     *              },
     *          ]
     *      }
     *      ```
     * @throws \Exception
     */
    public function getIamPolicy(): object
    {
        $uri = 'https://cloudresourcemanager.googleapis.com/v3/projects/' . $this->project_id . ':getIamPolicy';
        $response = $this->api_client->rest()->post(
            $uri,
            [
                'resource' => 'projects/' . $this->project_id
            ]
        );
        return $response->object;
    }

    /**
     * Use Google API to update existing IAM policy and bindings for a project
     *
     * @param object $policy
     *      Updated IAM policy with added or removed bindings or principals
     *      ```json
     *      {
     *          "version": 1,
     *          "etag": "A1b2+C3D4e5=",
     *          "bindings": [
     *              {
     *                  "role": "roles/compute.admin",
     *                  "members": [
     *                      "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
     *                  ],
     *              },
     *              {
     *                  "role": "roles/owner",
     *                  "members": [
     *                      "user:dmurphy@example.com",
     *                      "user:klibby@example.com"
     *                  ],
     *              },
     *          ]
     *      }
     *      ```
     *
     * @return object
     */
    protected function updateIamPolicy(object $policy)
    {
        $uri = 'https://cloudresourcemanager.googleapis.com/v3/projects/' . $this->project_id . ':setIamPolicy';
        $response = $this->api_client->rest()->post(
            $uri,
            [
                'policy' => $policy,
                'resource' => 'projects/' . $this->project_id
            ]
        );
        return $response;
    }

    /**
     * Attach an IAM user to a role using IAM Policy binding array
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @param string $iam_email
     *      User or service account email address
     *      ```
     *      (example) pcook@example.com
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @return bool
     *      ```
     *      (true) Successfully created
     *      (false) User and role already exists
     *      ```
     */
    public function attach(string $role_name, string $iam_email)
    {
        // TODO Add validation that GCP role exists
        // TODO Add validation that IAM email address is valid

        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        // Get current IAM policy for GCP project with array of current bindings
        $iam_policy = $this->getIamPolicy();

        // Add empty bindings array to IAM policy if it does not exist
        // Note: It is rare that this would not exist since built-in Google
        // services have IAM service accounts mapped to roles.
        if (!property_exists($iam_policy, 'bindings')) {
            $iam_policy->bindings = [];
        }

        // Verify if IAM policy already has binding for this user and role
        if ($this->checkIfEmailRoleBindingExists($iam_policy->bindings, $role_name, $iam_email) == true) {
            return false;
        }

        // Add new role object array to IAM policy if it does not exist
        if ($this->checkIfRoleBindingArrayExists($iam_policy->bindings, $role_name) == false) {
            $role_binding = $this->createRoleBindingDefinition($role_name);
            $iam_policy->bindings[] = $role_binding;
        }

        // Generate a new IAM policy object with added IAM email
        $updated_iam_policy = $this->addEmailToRoleInPolicyObject(
            $iam_policy,
            $role_name,
            $iam_email
        );

        $this->updateIamPolicy($updated_iam_policy);

        return true;
    }

    /**
     * Detach an IAM user from a role using IAM Policy binding array
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @param string $iam_email
     *      User or service account email address
     *      ```
     *      (example) pcook@example.com
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @return bool
     *      ```
     *      (true) Successfully removed
     *      (false) User did not exist
     *      ```
     */
    public function detach(string $role_name, string $iam_email)
    {
        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        // Get current IAM policy for GCP project with array of current bindings
        $iam_policy = $this->getIamPolicy();

        // Add empty bindings array to IAM policy if it does not exist
        // Note: It is rare that this would not exist since built-in Google
        // services have IAM service accounts mapped to roles.
        if (!property_exists($iam_policy, 'bindings')) {
            $iam_policy->bindings = [];
        }

        // Verify if IAM policy already has binding for this user and role
        if ($this->checkIfEmailRoleBindingExists($iam_policy->bindings, $role_name, $iam_email) == false) {
            return false;
        }

        // Generate a new IAM policy object with removed IAM email
        $updated_iam_policy = $this->removeEmailFromRoleInPolicyObject(
            $iam_policy,
            $role_name,
            $iam_email
        );

        unset($updated_iam_policy->etag);

        $this->updateIamPolicy($updated_iam_policy);

        return true;
    }

    /**
     * Get the array with the existing role binding and members
     *
     * @param array $bindings
     *      The current bindings array of the IAM policy for the project
     *      ```php
     *      [
     *          {
     *              "role": "roles/compute.admin",
     *              "members": [
     *                  "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
     *              ],
     *          },
     *          {
     *              "role": "roles/owner",
     *              "members": [
     *                  "user:dmurphy@example.com",
     *                  "user:klibby@example.com"
     *              ],
     *          },
     *      ]
     *      ```
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @return object
     *      ```php
     *      {
     *          'role' => 'roles/compute.admin',
     *          'members' => [
     *              'user:dmurphy@example.com'
     *          ]
     *      }
     *      ```
     */
    protected function getRoleBindingArray(array $bindings, string $role_name)
    {
        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        // If role does not exist in array, throw an exception.
        // Note: Based on single responsibility principle, you should use the
        // `checkIfRoleBindingArrayExists` and `createRoleBindingArray` methods.
        if (!$this->checkIfRoleBindingArrayExists($bindings, $role_name)) {
            // TODO Throw exception
        }

        // Get existing role binding
        $role_binding = collect($bindings)->where('role', $role_name);

        // Set variable for array key (int)
        $role_binding_key = key($role_binding->all());

        // Get array of members attached to role
        $role_binding_array = $role_binding->all()[$role_binding_key];

        return $role_binding_array;
    }

    /**
     * Create an array with the role binding and empty array of members
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @return object
     *      ```php
     *      {
     *          'role' => 'roles/compute.admin',
     *          'members' => []
     *      }
     *      ```
     */
    protected function createRoleBindingDefinition(string $role_name)
    {
        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        return (object) [
            'role' => $role_name,
            'members' => []
        ];
    }

    /**
     * Check if role exists in binding array
     *
     * @param array $bindings
     *      The current bindings array of the IAM policy for the project
     *      ```php
     *      [
     *          {
     *              "role": "roles/compute.admin",
     *              "members": [
     *                  "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
     *              ],
     *          },
     *          {
     *              "role": "roles/owner",
     *              "members": [
     *                  "user:dmurphy@example.com",
     *                  "user:klibby@example.com"
     *              ],
     *          },
     *      ]
     *      ```
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @return bool
     *      ```
     *      (true) Role binding array exists
     *      (false) Role binding array does not exist
     *      ```
     */
    protected function checkIfRoleBindingArrayExists(array $bindings, string $role_name): bool
    {
        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        return collect($bindings)->contains('role', $role_name);
    }

    /**
     * Check if binding exists for IAM email address and role
     *
     * Loops through the current bindings and compares to the request_data role and iam_email
     *
     * @param array $bindings
     *      The current bindings array of the IAM policy for the project
     *      ```php
     *      [
     *          {
     *              "role": "roles/compute.admin",
     *              "members": [
     *                  "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
     *              ],
     *          },
     *          {
     *              "role": "roles/owner",
     *              "members": [
     *                  "user:dmurphy@example.com",
     *                  "user:klibby@example.com"
     *              ],
     *          },
     *      ]
     *      ```
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @param string $iam_email
     *      User or service account email address
     *      ```
     *      (example) pcook@example.com
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @return bool
     *      ```
     *      (true) The role binding already exist for the user
     *      (false) The role binding does not exist for the user
     *      ```
     */
    protected function checkIfEmailRoleBindingExists(array $bindings, string $role_name, string $iam_email): bool
    {
        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        foreach ($bindings as $binding) {
            if ($binding->role == $role_name) {
                foreach ($binding->members as $member) {
                    if ($member == $iam_email) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Add an IAM email to a role binding members array in the IAM policy object
     *
     * @param object $policy
     *      The IAM policy for the GCP project. This can be returned from the
     *      `getIamPolicy` method or modified prior to calling this method if
     *      you need to add a new role using `createRoleBindingArray`.
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @param string $iam_email
     *      User or service account email address
     *      ```
     *      (example) pcook@example.com
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @return object
     *      The updated IAM policy object.
     *      ```json
     *      {
     *          "version": 1,
     *          "etag": "A1b2+C3D4e5=",
     *          "bindings": [
     *              {
     *                  "role": "roles/compute.admin",
     *                  "members": [
     *                      "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
     *                  ],
     *              },
     *              {
     *                  "role": "roles/owner",
     *                  "members": [
     *                      "user:dmurphy@example.com",
     *                      "user:klibby@example.com",
     *                      "user:pcook@example.com"
     *                  ],
     *              },
     *          ]
     *      }
     *      ```
     */
    protected function addEmailToRoleInPolicyObject(
        object $policy,
        string $role_name,
        string $iam_email
    ): object {
        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        // Create collection of bindings to allow searching
        $policy_bindings = collect($policy->bindings);

        // Get existing role binding
        $role_binding = $policy_bindings->where('role', $role_name);

        // Set variable for array key (int)
        $role_binding_key = key($role_binding->all());

        // Get array of members attached to role
        $role_binding_members = $role_binding->all()[$role_binding_key]->members;

        // If user already exists in members array, throw an exception.
        // Note: If you encounter this exception, you should call the
        // `checkIfEmailRoleBindingExists` method before this method.
        if (in_array($iam_email, $role_binding_members)) {
            // TODO Throw exception
        }

        // Add IAM email to array
        $role_binding_members[] = $iam_email;

        // Update members array for role binding
        $policy->bindings[$role_binding_key]->members = $role_binding_members;

        return $policy;
    }

    /**
     * Remove an IAM email from a role binding members array in the IAM policy
     *
     * @param object $policy
     *      The IAM policy for the GCP project. This can be returned from the
     *      `getIamPolicy` method or modified prior to calling this method if
     *      you need to add a new role using `createRoleBindingArray`.
     *
     * @param string $role_name
     *      The GCP role name with prefixed `roles/`
     *      ```
     *      (example) roles/compute.admin
     *      ```
     *
     * @param string $iam_email
     *      User or service account email address
     *      ```
     *      (example) klibby@example.com
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @return object
     *      The updated IAM policy object.
     *      ```json
     *      {
     *          "version": 1,
     *          "etag": "A1b2+C3D4e5=",
     *          "bindings": [
     *              {
     *                  "role": "roles/compute.admin",
     *                  "members": [
     *                      "serviceAccount:hackystack-provisioner@my-project.iam.gserviceaccount.com",
     *                  ],
     *              },
     *              {
     *                  "role": "roles/owner",
     *                  "members": [
     *                      "user:dmurphy@example.com"
     *                  ],
     *              },
     *          ]
     *      }
     *      ```
     */
    protected function removeEmailFromRoleInPolicyObject(
        object $policy,
        string $role_name,
        string $iam_email
    ): object {
        // Check if role has prefix of `roles/` and add if doesn't exist
        if (!Str::startsWith($role_name, 'roles/')) {
            $role_name = 'roles/' . $role_name;
        }

        // Create collection of bindings to allow searching
        $policy_bindings = collect($policy->bindings);

        // Get existing role binding
        $role_binding = $policy_bindings->where('role', $role_name);

        // Set variable for array key (int)
        $role_binding_key = key($role_binding->all());

        // Get array of members attached to role
        $role_binding_members = $role_binding->all()[$role_binding_key]->members;

        // If user does not exist in members array, throw an exception.
        // Note: If you encounter this exception, you should call the
        // `checkIfEmailRoleBindingExists` method before this method.
        if (!in_array($iam_email, $role_binding_members)) {
            // TODO Throw exception
        }

        // Get array key for principal
        $role_binding_member_key = array_search($iam_email, $role_binding_members);

        // Remove the member from the array
        unset($role_binding_members[$role_binding_member_key]);

        // Update members array for role binding
        $policy->bindings[$role_binding_key]->members = $role_binding_members;

        return $policy;
    }
}
