<?php

namespace App\Services\V1\Vendor\Gcp;

use App\Services\V1\Vendor\Gcp\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudBillingProjectService
{

    use BaseService;

    /**
     *   Initialize the Google API Client
     *
     *   @see https://developers.google.com/identity/protocols/oauth2/scopes
     *   @see https://github.com/googleapis/google-api-php-client-services/blob/master/src/Google/Service/Cloudbilling.php
     *
     *   @param uuid    $cloud_provider_id
     */
    public function __construct($cloud_provider_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);
        $this->setGoogleApiClient();

        // Add API Scope(s)
        $this->google_api_client->addScope(\Google_Service_Cloudbilling::CLOUD_BILLING);
    }

    /**
     *   Get the billing account information for the project
     *
     *   @see https://github.com/googleapis/google-cloud-php/blob/master/Billing/src/V1/ProjectBillingInfo.php
     *   @see https://github.com/googleapis/google-api-php-client-services/blob/master/src/Cloudbilling/Resource/Projects.php#L42
     *
     *   @param  array  $request_data
     *      project_name|string          Ex. projects/my-project-name-a1b2c3d4 or my-project-name-a1b2c3d4
     *
     *   @return array  API Meta Data (Response) with a billing account
     *      billingAccountName: "billingAccounts/A1B2C3D4-E5F7A8-B9C0D1"
     *      billingEnabled: true
     *      name: "projects/my-project-name-a1b2c3d4/billingInfo"
     *      projectId: "my-project-name-a1b2c3d4"
     *
     *   @return array  API Meta Data (Response) without a billing account
     *      billingAccountName: ""
     *      billingEnabled: false
     *      name: "projects/my-project-name-a1b2c3d4/billingInfo"
     *      projectId: "my-project-name-a1b2c3d4"
     */
    public function get($request_data = [])
    {
        // Initialize the Cloud Billing API Service
        $google_cloud_billing_service = new \Google_Service_Cloudbilling($this->google_api_client);

        // Check if project has required prefix of `projects/`
        if(Str::startsWith($request_data['project_name'], 'projects/')) {
            $project_name = $request_data['project_name'];
        } else {
            $project_name = 'projects/'.$request_data['project_name'];
        }

        // Get the Billing Account information for the Project
        try {
            $billing_api_response = $google_cloud_billing_service->projects->getBillingInfo($project_name, []);
        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        return $billing_api_response;

    }

    /**
     *   Update the billing account information for the project
     *
     *   @see https://github.com/googleapis/google-cloud-php/blob/master/Billing/src/V1/ProjectBillingInfo.php
     *   @see https://github.com/googleapis/google-cloud-php/blob/master/Billing/src/V1/UpdateProjectBillingInfoRequest.php
     *   @see https://github.com/googleapis/google-api-php-client-services/blob/master/src/Cloudbilling/Resource/Projects.php#L81
     *
     *   @param  array  $request_data [description]
     *      project_name|string             Ex. projects/my-project-name-a1b2c3d4 or my-project-name-a1b2c3d4
     *      billing_account_id|string       Ex. billingAccounts/A1B2C3D4-E5F7A8-B9C0D1 or A1B2C3D4-E5F7A8-B9C0D1
     *
     *   @return array  API Meta Data (Response) with a billing account
     *      billingAccountName: "billingAccounts/017B02-778F9C-493B83",
     *      billingEnabled: true,
     *      name: "projects/achueshev-4ed36838/billingInfo",
     *      projectId: "achueshev-4ed36838",
     */
    public function update($request_data = [])
    {
        // Initialize the Cloud Billing API Service
        $google_cloud_billing_service = new \Google_Service_Cloudbilling($this->google_api_client);

        // Check if project has required prefix of `projects/`
        if(Str::startsWith($request_data['project_name'], 'projects/')) {
            $project_name = $request_data['project_name'];
        } else {
            $project_name = 'projects/'.$request_data['project_name'];
        }

        // Check if billing account has required prefix of `billingAccounts/`
        if(Str::startsWith($request_data['billing_account_id'], 'billingAccounts/')) {
            $billing_account_id = $request_data['billing_account_id'];
        } else {
            $billing_account_id = 'billingAccounts/'.$request_data['billing_account_id'];
        }

        // Define API request body
        $request_body = new \Google_Service_Cloudbilling_ProjectBillingInfo([
            'billing_account_name' => $billing_account_id
        ]);

        // Get the Billing Account information for the Project
        try {
            $billing_api_response = $google_cloud_billing_service->projects->updateBillingInfo($project_name, $request_body, []);
        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        return $billing_api_response;

    }

}
