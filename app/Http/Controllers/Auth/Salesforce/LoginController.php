<?php

namespace App\Http\Controllers\Auth\Salesforce;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{
    public function __construct()
    {
        //
    }

    public function redirectToProvider(Request $request)
    {
        return null;
    }

    public function handleProviderCallback(Request $request)
    {
        return null;
    }

}
